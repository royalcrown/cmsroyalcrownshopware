<?php declare(strict_types=1);

namespace CmsRoyalCrownShopware\DataResolver;

use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\FieldConfig;
use Shopware\Core\Content\Cms\DataResolver\FieldConfigCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\Struct\ImageStruct;
use Shopware\Core\Content\Media\Cms\AbstractDefaultMediaResolver;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Struct\ArrayStruct;

class DoubleImageCmsElementResolver extends AbstractCmsElementResolver
{
    public function __construct(private readonly AbstractDefaultMediaResolver $mediaResolver)
    {
    }

    public function getType(): string
    {
        return 'double-image';
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
//        dd($slot->getFieldConfig()->get('mediaRight'));
        $mediaRightConfig = $slot->getFieldConfig()->get('mediaRight');
        if (
            $mediaRightConfig === null
            || $mediaRightConfig->isMapped()
            || $mediaRightConfig->isDefault()
            || $mediaRightConfig->getValue() === null
        ) {
            return null;
        }
        $mediaLeftConfig = $slot->getFieldConfig()->get('mediaLeft');
        if (
            $mediaLeftConfig === null
            || $mediaLeftConfig->isMapped()
            || $mediaLeftConfig->isDefault()
            || $mediaLeftConfig->getValue() === null
        ) {
            return null;
        }

        $criteria = new Criteria([$mediaLeftConfig->getStringValue(), $mediaRightConfig->getStringValue()]);

        $criteriaCollection = new CriteriaCollection();
        $criteriaCollection->add('media_' . $slot->getUniqueIdentifier(), MediaDefinition::class, $criteria);

        return $criteriaCollection;
    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {
        $config = $slot->getFieldConfig();
        $imageLeft = new ImageStruct();
        $imageRight = new ImageStruct();
        $data = new ArrayStruct([$imageLeft, $imageRight]);
        $slot->setData($data);
        $this->setUrlConfigValues($config, $imageLeft, $resolverContext, true);
        $this->setUrlConfigValues($config, $imageRight, $resolverContext, false);
        $this->setMediaConfig($config, $slot, $imageLeft, $result, $resolverContext, true);
        $this->setMediaConfig($config, $slot, $imageRight, $result, $resolverContext, false);
    }

    private function setUrlConfigValues(
        FieldConfigCollection     $config,
        ImageStruct     $imageStruct,
        ResolverContext $resolverContext,
        bool            $isLeft
    )
    {
        if($isLeft) {
            $newTabConfig = $config->get('newTabLeft');
            $urlConfig = $config->get('leftUrl');
        } else {
            $newTabConfig = $config->get('newTabRight');
            $urlConfig = $config->get('rightUrl');
        }

        if($urlConfig !== null) {
            if ($urlConfig->isStatic()) {
                $imageStruct->setUrl($urlConfig->getStringValue());

            }

            if ($urlConfig->isMapped() && $resolverContext instanceof EntityResolverContext) {
                $url = $this->resolveEntityValue($resolverContext->getEntity(), $urlConfig->getStringValue());
                if ($url) {
                    $imageStruct->setUrl($url);
                }
            }
            if ($newTabConfig !== null) {
                $imageStruct->setNewTab($newTabConfig->getBoolValue());
            }
        }
    }

    private function setMediaConfig(FieldConfigCollection $config, CmsSlotEntity $slot, ImageStruct $imageStruct, ElementDataCollection $result, ResolverContext $resolverContext, bool $isLeft) {
        if($isLeft) {
            $mediaConfig = $config->get('mediaLeft');
        } else {
            $mediaConfig = $config->get('mediaRight');
        }

        if ($mediaConfig && $mediaConfig->getValue()) {
            $this->addMediaEntity($slot, $imageStruct, $result, $mediaConfig, $resolverContext);
        }

    }

    private function addMediaEntity(
        CmsSlotEntity         $slot,
        ImageStruct           $image,
        ElementDataCollection $result,
        FieldConfig           $config,
        ResolverContext       $resolverContext
    ): void
    {
        if ($config->isDefault()) {
            $media = $this->mediaResolver->getDefaultCmsMediaEntity($config->getStringValue());

            if ($media) {
                $image->setMedia($media);
            }
        }

        if ($config->isMapped() && $resolverContext instanceof EntityResolverContext) {
            $media = $this->resolveEntityValue($resolverContext->getEntity(), $config->getStringValue());

            if ($media instanceof MediaEntity) {
                $image->setMediaId($media->getUniqueIdentifier());
                $image->setMedia($media);
            }
        }

        if ($config->isStatic()) {
            $image->setMediaId($config->getStringValue());

            $searchResult = $result->get('media_' . $slot->getUniqueIdentifier());
            if (!$searchResult) {
                return;
            }

            $media = $searchResult->get($config->getStringValue());
            if (!$media instanceof MediaEntity) {
                return;
            }
//            $media->setUrl('');
            $image->setMedia($media);
        }
    }
}
