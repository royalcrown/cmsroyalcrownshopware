// Elements
import './module/sw-cms/elements/custom-image';
import './module/sw-cms/elements/custom-text';
import './module/sw-cms/elements/form-override';
import './module/sw-cms/elements/accordion'
// Blocks
import './module/sw-cms/blocks/dynamicBlocks/two-grid-block-3-9';
import './module/sw-cms/blocks/dynamicBlocks/two-grid-block-4-8';
import './module/sw-cms/blocks/dynamicBlocks/two-grid-block-6-6';
import './module/sw-cms/blocks/dynamicBlocks/two-grid-block-8-4';
import './module/sw-cms/blocks/dynamicBlocks/two-grid-block-9-3';
import './module/sw-cms/blocks/dynamicBlocks/three-grid-block-3-3-6';
// import './module/sw-cms/blocks/dynamicBlocks/three-grid-block-3-4-5';
// import './module/sw-cms/blocks/dynamicBlocks/three-grid-block-3-5-4';
import './module/sw-cms/blocks/dynamicBlocks/three-grid-block-3-6-3';
import './module/sw-cms/blocks/dynamicBlocks/three-grid-block-4-4-4';
import './module/sw-cms/blocks/dynamicBlocks/three-grid-block-6-3-3';
import './module/sw-cms/blocks/dynamicBlocks/four-grid-block-3-3-3-3';

//Components
import './module/sw-cms/component/sw-cms-sidebar';
import './module/sw-cms/component/sw-cms-block-layout-config';
