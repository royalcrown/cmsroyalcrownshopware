import template from './rc-cms-preview-four-grid-block-3-3-3-3.html.twig';
import './rc-cms-preview-four-grid-block-3-3-3-3.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-four-grid-block-3-3-3-3', {
    template
});
