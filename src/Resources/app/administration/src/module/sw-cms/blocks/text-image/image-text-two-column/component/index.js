import template from './sw-cms-block-image-text-two-column.html.twig';
import './sw-cms-block-image-text-two-column.scss';

Shopware.Component.register('sw-cms-block-image-text-two-column', {
    template
});
