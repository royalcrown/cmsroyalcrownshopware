import template from './rc-cms-block-three-grid-block-6-3-3.html.twig';
import './rc-cms-block-three-grid-block-6-3-3.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-three-grid-block-6-3-3', {
    template
});
