import template from './rc-cms-preview-two-grid-block-9-3.html.twig';
import './rc-cms-preview-two-grid-block-9-3.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-two-grid-block-9-3', {
    template
});
