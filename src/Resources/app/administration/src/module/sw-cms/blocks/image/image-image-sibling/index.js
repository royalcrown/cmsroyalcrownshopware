import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-image-sibling',
    label: 'rc-cms.blocks.image.image-image',
    category: 'image',
    component: 'sw-cms-block-image-image-sibling',
    previewComponent: 'sw-cms-preview-image-image-sibling',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        center: {
            type: 'double-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    mediaLeft: {
                        value: 'framework/assets/default/cms/preview_mountain_large.jpg',
                        source: 'default',
                    },
                    mediaRight: {
                        value: 'framework/assets/default/cms/preview_plant_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
    },
});
