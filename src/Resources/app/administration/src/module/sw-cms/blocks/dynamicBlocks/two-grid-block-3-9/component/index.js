import template from './rc-cms-block-two-grid-block-3-9.html.twig';
import './rc-cms-block-two-grid-block-3-9.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-two-grid-block-3-9', {
    template
});
