import template from './sw-cms-preview-image-image-sibling.html.twig';
import './sw-cms-preview-image-image-sibling.scss';

Shopware.Component.register('sw-cms-preview-image-image-sibling', {
    template
});