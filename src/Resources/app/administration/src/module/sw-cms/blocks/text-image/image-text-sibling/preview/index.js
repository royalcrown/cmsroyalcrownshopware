import template from './sw-cms-preview-image-text-sibling.html.twig';
import './sw-cms-preview-image-text-sibling.scss';

Shopware.Component.register('sw-cms-preview-image-text-sibling', {
    template
});
