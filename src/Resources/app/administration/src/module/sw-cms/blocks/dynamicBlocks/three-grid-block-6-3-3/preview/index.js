import template from './rc-cms-preview-three-grid-block-6-3-3.html.twig';
import './rc-cms-preview-three-grid-block-6-3-3.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-three-grid-block-6-3-3', {
    template
});
