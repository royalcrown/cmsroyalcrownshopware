import template from './rc-cms-block-three-grid-block-3-4-5.html.twig';
import './rc-cms-block-three-grid-block-3-4-5.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-three-grid-block-3-4-5', {
    template
});
