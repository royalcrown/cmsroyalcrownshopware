import template from './sw-cms-block-image-text-sibling.html.twig';
import './sw-cms-block-image-text-sibling.scss';

Shopware.Component.register('sw-cms-block-image-text-sibling', {
    template
});
