import template from './rc-cms-preview-two-grid-block-4-8.html.twig';
import './rc-cms-preview-two-grid-block-4-8.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-two-grid-block-4-8', {
    template
});
