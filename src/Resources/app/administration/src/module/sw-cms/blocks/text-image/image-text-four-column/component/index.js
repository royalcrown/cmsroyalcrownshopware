import template from './sw-cms-block-image-text-four-column.html.twig';
import './sw-cms-block-image-text-four-column.scss';

Shopware.Component.register('sw-cms-block-image-text-four-column', {
    template
});
