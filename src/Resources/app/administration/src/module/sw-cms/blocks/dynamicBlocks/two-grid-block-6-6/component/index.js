import template from './rc-cms-block-two-grid-block-6-6.html.twig';
import './rc-cms-block-two-grid-block-6-6.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-two-grid-block-6-6', {
    template
});
