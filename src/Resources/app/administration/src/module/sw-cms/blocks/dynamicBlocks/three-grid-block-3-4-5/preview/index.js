import template from './rc-cms-preview-three-grid-block-3-4-5.html.twig';
import './rc-cms-preview-three-grid-block-3-4-5.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-three-grid-block-3-4-5', {
    template
});
