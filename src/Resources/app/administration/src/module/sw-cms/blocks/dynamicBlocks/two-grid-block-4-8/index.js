import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'two-grid-block-4-8',
    label: 'rc-cms.blocks.grid.label.4-8',
    category: 'grid-blocks',
    component: 'sw-cms-block-two-grid-block-4-8',
    previewComponent: 'sw-cms-preview-two-grid-block-4-8',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        left: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_camera_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        right: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_mountain_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
    },
});
