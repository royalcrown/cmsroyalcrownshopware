import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-text-two-column',
    label: 'rc-cms.blocks.image-text.2-col-image-text',
    category: 'text-image',
    component: 'sw-cms-block-image-text-two-column',
    previewComponent: 'sw-cms-preview-image-text-two-column',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        left: {
            type: 'image-link',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_camera_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        right: {
            type: 'image-link',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_mountain_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
    },
});
