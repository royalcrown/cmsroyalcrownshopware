import template from './sw-cms-preview-image-text-two-column.html.twig';
import './sw-cms-preview-image-text-two-column.scss';

Shopware.Component.register('sw-cms-preview-image-text-two-column', {
    template
});
