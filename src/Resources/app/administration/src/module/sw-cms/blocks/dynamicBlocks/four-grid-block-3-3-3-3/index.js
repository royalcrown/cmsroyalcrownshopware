import './component';
import './preview';
const { Service } = Shopware;
Service('cmsService').registerCmsBlock({
    name: 'four-grid-block-3-3-3-3',
    label: 'rc-cms.blocks.grid.label.3-3-3-3',
    category: 'grid-blocks',
    component: 'sw-cms-block-four-grid-block-3-3-3-3',
    previewComponent: 'sw-cms-preview-four-grid-block-3-3-3-3',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        left: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_camera_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        midLeft: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_plant_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        midRight: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_glasses_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        right: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_mountain_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
    },
});
