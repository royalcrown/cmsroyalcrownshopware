import template from './sw-cms-block-image-image-sibling.html.twig';
import './sw-cms-block-image-image-sibling.scss';

Shopware.Component.register('sw-cms-block-image-image-sibling', {
    template
});
