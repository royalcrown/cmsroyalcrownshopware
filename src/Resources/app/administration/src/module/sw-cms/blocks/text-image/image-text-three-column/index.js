import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-text-three-column',
    label: 'rc-cms.blocks.image-text.3-col-image-text-with-overlay',
    category: 'text-image',
    component: 'sw-cms-block-image-text-three-column',
    previewComponent: 'sw-cms-preview-image-text-three-column',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        left: {
            type: 'image-text-overlay',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_camera_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        center: {
            type: 'image-text-overlay',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_plant_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        right: {
            type: 'image-text-overlay',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_mountain_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
    },
});
