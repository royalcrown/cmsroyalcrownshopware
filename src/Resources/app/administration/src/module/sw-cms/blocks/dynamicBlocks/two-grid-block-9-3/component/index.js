import template from './rc-cms-block-two-grid-block-9-3.html.twig';
import './rc-cms-block-two-grid-block-9-3.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-two-grid-block-9-3', {
    template
});
