import template from './sw-cms-preview-image-text-four-column.html.twig';
import './sw-cms-preview-image-text-four-column.scss';

Shopware.Component.register('sw-cms-preview-image-text-four-column', {
    template
});
