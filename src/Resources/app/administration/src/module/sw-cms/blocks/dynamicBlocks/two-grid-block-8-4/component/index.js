import template from './rc-cms-block-two-grid-block-8-4.html.twig';
import './rc-cms-block-two-grid-block-8-4.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-two-grid-block-8-4', {
    template
});
