import template from './rc-cms-preview-two-grid-block-6-6.html.twig';
import './rc-cms-preview-two-grid-block-6-6.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-two-grid-block-6-6', {
    template
});
