import './component';
import './preview';
const { Service } = Shopware;
Service('cmsService').registerCmsBlock({
    name: 'three-grid-block-3-4-5',
    label: 'rc-cms.blocks.grid.label.3-4-5',
    category: 'grid-blocks',
    component: 'sw-cms-block-three-grid-block-3-4-5',
    previewComponent: 'sw-cms-preview-three-grid-block-3-4-5',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        left: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_camera_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        center: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_glasses_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
        right: {
            type: 'custom-image',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_mountain_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
    },
});
