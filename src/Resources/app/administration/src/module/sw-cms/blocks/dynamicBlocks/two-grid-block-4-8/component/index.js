import template from './rc-cms-block-two-grid-block-4-8.html.twig';
import './rc-cms-block-two-grid-block-4-8.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-two-grid-block-4-8', {
    template
});
