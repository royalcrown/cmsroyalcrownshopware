import template from './sw-cms-preview-image-text-three-column.html.twig';
import './sw-cms-preview-image-text-three-column.scss';

Shopware.Component.register('sw-cms-preview-image-text-three-column', {
    template
});
