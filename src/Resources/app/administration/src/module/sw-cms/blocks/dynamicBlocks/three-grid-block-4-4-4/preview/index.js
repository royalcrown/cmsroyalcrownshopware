import template from './rc-cms-preview-three-grid-block-4-4-4.html.twig';
import './rc-cms-preview-three-grid-block-4-4-4.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-three-grid-block-4-4-4', {
    template
});
