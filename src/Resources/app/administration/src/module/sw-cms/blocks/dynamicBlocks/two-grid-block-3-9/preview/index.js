import template from './rc-cms-preview-two-grid-block-3-9.html.twig';
import './rc-cms-preview-two-grid-block-3-9.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-two-grid-block-3-9', {
    template
});
