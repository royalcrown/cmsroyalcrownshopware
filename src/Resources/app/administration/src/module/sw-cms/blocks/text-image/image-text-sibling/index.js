import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-text-sibling',
    label: 'rc-cms.blocks.image-text.image-text-optional-button',
    category: 'text-image',
    component: 'sw-cms-block-image-text-sibling',
    previewComponent: 'sw-cms-preview-image-text-sibling',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        center: {
            type: 'image-text-sibling',
            default: {
                config: {
                    displayMode: { source: 'static', value: 'cover' },
                },
                data: {
                    media: {
                        value: 'framework/assets/default/cms/preview_camera_large.jpg',
                        source: 'default',
                    },
                },
            },
        },
    },
});
