import template from './rc-cms-preview-two-grid-block-8-4.html.twig';
import './rc-cms-preview-two-grid-block-8-4.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-two-grid-block-8-4', {
    template
});
