import template from './rc-cms-block-three-grid-block-3-5-4.html.twig';
import './rc-cms-block-three-grid-block-3-5-4.scss';
import '../../grid-styles/base.scss';

Shopware.Component.register('sw-cms-block-three-grid-block-3-5-4', {
    template
});
