import template from './rc-cms-preview-three-grid-block-3-3-6.html.twig';
import './rc-cms-preview-three-grid-block-3-3-6.scss';

const { Component } = Shopware;
Component.register('sw-cms-preview-three-grid-block-3-3-6', {
    template
});
