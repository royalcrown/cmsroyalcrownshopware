import template from './sw-cms-block-image-text-three-column.html.twig';
import './sw-cms-block-image-text-three-column.scss';

Shopware.Component.register('sw-cms-block-image-text-three-column', {
    template
});
