import template from './sw-cms-block-layout-config.html.twig';
import './sw-cms-block-layout-config.scss';

const {Component} = Shopware;
Component.override('sw-cms-block-layout-config', {
    template
});
