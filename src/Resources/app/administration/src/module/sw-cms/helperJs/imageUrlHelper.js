import CMS from "../constant/sw-cms.constant";
const mediaUrlHelper = (element, assetFilter) => {
    const fallBackImageFileName = CMS.MEDIA.previewMountain.slice(CMS.MEDIA.previewMountain.lastIndexOf('/') + 1);
    const staticFallBackImage = assetFilter(`administration/static/img/cms/${fallBackImageFileName}`);
    const elemData = element.data.media;
    const elemConfig = element.config.media;

    if (elemConfig.source === 'mapped') {
        const demoMedia = this.getDemoValue(elemConfig.value);

        if (demoMedia?.url) {
            return demoMedia.url;
        }

        return staticFallBackImage;
    }

    if (elemConfig.source === 'default') {
        // use only the filename
        const fileName = elemConfig.value.slice(elemConfig.value.lastIndexOf('/') + 1);
        return assetFilter(`/administration/static/img/cms/${fileName}`);
    }

    if (elemData?.id) {
        return element.data.media.url;
    }

    if (elemData?.url) {
        return assetFilter(elemConfig.url);
    }
    return staticFallBackImage;
}
export { mediaUrlHelper };