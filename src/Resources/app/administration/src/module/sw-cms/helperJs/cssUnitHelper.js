const CssUnits =  {
    PX : 'px',
    REM : 'rem',
    PERCENTAGE : '%'
}

const getCssUnitFromString = (value) => {
    return  value.replace(/[^a-zA-Z%]/g, "").toLowerCase();
}

const isCssUnitPx = (value) => {
    return getCssUnitFromString(value) === CssUnits.PX;
}
const isCssUnitAcceptable = (value) => {
    return value === CssUnits.PERCENTAGE || value === CssUnits.REM || value === CssUnits.PX;
}

export { getCssUnitFromString, isCssUnitAcceptable, isCssUnitPx }