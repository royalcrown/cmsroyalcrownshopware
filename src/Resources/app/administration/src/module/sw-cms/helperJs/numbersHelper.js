const getNumbersFromString = (value) => {
   return Number(value.replace(/[^0-9]/g, ""));
}

export {getNumbersFromString}