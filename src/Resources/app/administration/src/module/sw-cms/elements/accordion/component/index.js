import template from './sw-cms-el-accordion.html.twig';
import './sw-cms-el-accordion.scss';

const { Mixin, Component } = Shopware;

Component.register('sw-cms-el-accordion', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        // buttonStyles(event) {
        //     console.log(event);
        //     // console.log(this.element.config.entries.value[index]);
        //     // console.log(this.element.config.entries.value);
        //     return '';
        //
        //     // return {
        //     // 'background-color': this.element.config.entries.value[index].btnBackground,
        //     // 'color':  this.element.config.entries.value[index].btnColor ,
        //     // 'border-radius':  this.element.config.entries.value[index].btnRadius ,
        //     // 'font-size':  this.element.config.entries.value[index].fontSize ,
        //     // 'font-weight':  this.element.config.entries.value[index].fontWeight
        //     // }
        // },
        // panelStyles(item) {
        //     console.log(item);
        //     return '';
        //     // return {
        //     //     'background-color': this.element.config.entries.value[index].panelBackground
        //     // }
        // }
    },
    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.initElementConfig('accordion');
        },
        buttonStyles(index) {
            return {
            'background-color': this.element.config.entries.value[index].btnBackground,
            'color':  this.element.config.entries.value[index].btnColor ,
            'border-radius':  this.element.config.entries.value[index].btnRadius ,
            'font-size':  this.element.config.entries.value[index].fontSize ,
            'font-weight':  this.element.config.entries.value[index].fontWeight
            }
        },
    },
});