import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'accordion',
    label: 'rc-cms.elements.accordion',
    component: 'sw-cms-el-accordion',
    configComponent: 'sw-cms-el-config-accordion',
    previewComponent: 'sw-cms-el-preview-accordion',
    defaultConfig: {
        entries: {
            source: 'static',
            value: [{
                title: null,
                content: null,
                btnBackground: 'transparent',
                btnColor: '#000',
                panelBackground: '#fff',
                btnRadius: '0 0 0 0',
                verticalAlign: null,
                fontSize: '0.87rem',
                fontWeight: '400'
            }],
        },
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '250px',
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        backgroundColor: {
            source: 'static',
            value: '#fffff00',
        },
        buttonColor: {
            source: 'static',
            value: '#ccc'
        },
        buttonTextColor: {
            source: 'static',
            value: '#000'
        },
        useTextLink: {
            source: 'static',
            value: false
        },
        useButton: {
            source: 'static',
            value: false
        },
        useImage: {
            source: 'static',
            value: false
        },
        linkOrButtonText: {
            source: 'static',
            value: null,
        },
        fontSize: {
            source: 'static',
            value: '0.78rem'
        },
        fontWeight: {
            source: 'static',
            value: '400',
        },
        linkColor: {
            source: 'static',
            value: '#000'
        },
        linkPosition: {
            source: 'static',
            value: 'center'
        },
        currentIndex: {
            source: 'static',
            value: 0
        },
        item: {
            source: 'static',
            value: {
                title: null,
                content: null
            }
        }
    },
});
