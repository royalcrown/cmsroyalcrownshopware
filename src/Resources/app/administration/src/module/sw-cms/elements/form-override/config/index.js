import template from './sw-cms-el-config-custom-form.html.twig';

const { Component } = Shopware;

Component.override('sw-cms-el-config-form', {
    template,
    });
