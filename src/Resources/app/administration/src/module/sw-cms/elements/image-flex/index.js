import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'image-flex',
    label: 'rc-cms.elements.image-flex-link',
    component: 'sw-cms-el-image-flex',
    configComponent: 'sw-cms-el-config-image-flex',
    previewComponent: 'sw-cms-el-preview-image-flex',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media',
            },
        },
        displayMode: {
            source: 'static',
            value: 'standard',
        },
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '250px',
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        linkColor: {
            source: 'static',
            value: '#062535'
        },
        text: {
            source: 'static',
            value: ''
        },
        imageDisplayType: {
            source: 'static',
            value: 'standard'
        },
        fontSize: {
            source: 'static',
            value: '1rem'
        },
        fontWeight: {
            source: 'static',
            value: '400',
        },

        textAlign: {
            source: 'static',
            value: 'text-bottom left'
        },
        imageWidth: {
            source: 'static',
            value: '50%'
        },
        secondImageWidth: {
            source: 'static',
            value: '50%'
        },
        textMarginTop: {
            source: 'static',
            value: null
        },
        textMarginRight: {
            source: 'static',
            value: null
        },
        textMarginLeft: {
            source: 'static',
            value: null
        },
        textMarginBottom: {
            source: 'static',
            value: null
        }
    },
});
