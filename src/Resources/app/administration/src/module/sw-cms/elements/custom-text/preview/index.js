import template from './sw-cms-el-preview-custom-text.html.twig';
import './sw-cms-el-preview-custom-text.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-custom-text', {template});