import template from './sw-cms-el-config-custom-image.html.twig';
import './sw-cms-el-config-custom-image.scss';
import {getNumbersFromString} from "../../../helperJs/numbersHelper";
import * as helper from "../../../helperJs/textPositionerHelper";
import {getCssUnitFromString, isCssUnitAcceptable} from "../../../helperJs/cssUnitHelper";
const { Mixin, Component } = Shopware;

Component.register('sw-cms-el-config-custom-image', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
            useTextOverlay: false,
            useTextLink: false,
            displayImageAsFigure: false,
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        previewSource() {
            if (this.element?.data?.media?.id) {
                return this.element.data.media;
            }

            return this.element.config.media.value;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('custom-image');
            this.useTextOverlay = this.element.config.useTextOverlay.value;
            this.useTextLink = this.element.config.useTextLink.value;
            this.displayImageAsFigure = this.element.config.displayImageAsFigure.value;
        },

        async onImageUpload({ targetId }) {
            const mediaEntity = await this.mediaRepository.get(targetId);

            this.element.config.media.value = mediaEntity.id;
            this.element.config.media.source = 'static';

            this.updateElementData(mediaEntity);

            this.$emit('element-update', this.element);
        },

        onImageRemove() {
            this.element.config.media.value = null;

            this.updateElementData();

            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
        },

        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];
            this.element.config.media.value = media.id;
            this.element.config.media.source = 'static';

            this.updateElementData(media);

            this.$emit('element-update', this.element);
        },

        updateElementData(media = null) {
            const mediaId = media === null ? null : media.id;
            if (!this.element.data) {
                this.$set(this.element, 'data', { mediaId, media });
            } else {
                this.$set(this.element.data, 'mediaId', mediaId);
                this.$set(this.element.data, 'media', media);
            }
        },

        onOpenMediaModal() {
            this.mediaModalIsOpen = true;
        },

        onChangeMinHeight(value) {
            if (getNumbersFromString(value) > 49){
                this.element.config.minHeight.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeSlideDirection(value) {
            this.element.config.slideDirection.value = value;
            this.$emit('element-update', this.element);
        },

        onChangeImageDisplayType(value) {
            this.element.config.imageDisplayType.value = value;
            this.$emit('element-update', this.element);
        },

        onBlur(content) {
            this.emitChanges(content);
        },
        onInput(content) {
            const el = document.createElement('div');
            el.innerHTML = content;
            const titleElements = el.querySelectorAll('h1, h2, h3, h4, h5, h6');
            titleElements.forEach(headerEl => el.removeChild(headerEl));
            const lastFontElement = Array.prototype.slice.call(el.querySelectorAll('FONT')).pop();

            if(lastFontElement) {
                if (this.element.config.ellipsisColor.value !== lastFontElement.getAttribute('color')) {
                    this.element.config.ellipsisColor.value = lastFontElement.getAttribute('color');
                }
            }
            if (content !== this.element.config.text.value) {
                this.element.config.text.value = content;
            }
            this.$emit('element-update', this.element);
        },
        onChangeSliderHeightText(value) {
            this.element.config.sliderHeight.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeVisibleLines(value) {
            this.element.config.amountOfLinesVisible.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeButtonText(value) {
            this.element.config.buttonText.value = value;
            this.$emit('element-update', this.element);
        },
        emitChanges(content) {
            if (content !== this.element.config.text.value) {
                this.element.config.text.value = content;
                this.$emit('element-update', this.element);
            }
        },
        onChangeUseTextOverlay(value) {
            this.useTextOverlay = value
            this.element.config.useTextOverlay.value = value;
            if(this.useTextLink) {
                this.useTextLink = !value
                this.element.config.useTextLink.value = !value;
            }
            this.$emit('element-update', this.element);
        },
        onChangeUseTextLink(value) {
            this.useTextLink = value
            this.element.config.useTextLink.value = value;
            if(this.useTextOverlay) {
                this.element.config.useTextOverlay.value = !value;
                this.useTextOverlay = !value;
            }
            this.$emit('element-update', this.element);
        },
        onChangeImageVerticalPosition(value) {
            this.element.config.imageVerticalPosition.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageHorizontalPosition(value) {
            this.element.config.imageHorizontalPosition.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeDisplayImageAsFigure(value) {
            if(!value && this.element.config.imageDisplayFigure.value !== 'normal') {
                this.onChangeImageDisplayFigure('normal');
            }
            this.element.config.displayImageAsFigure.value = value;
            this.displayImageAsFigure = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageDisplayFigure(value) {
            const width = document.getElementById(this.element.id).getBoundingClientRect().width;
            const height = document.getElementById(this.element.id).getBoundingClientRect().height;
            if (value !== 'normal') {
                if(this.element.config.imageDisplayType.value === 'standard') {
                    this.element.config.imageDisplayType.value = 'cover';
                    this.$emit('element-update', this.element);
                }
                if(height > width) {
                    this.element.config.minHeight.value = `${document.getElementById(this.element.id).getBoundingClientRect().width}px`;
                } else {
                    if(height !== this.element.config.minHeight.value) {
                        this.element.config.minHeight.value = `${height}px`;
                    }
                    this.element.config.width.value = `${document.getElementById(this.element.id).getBoundingClientRect().height}px`;
                }
            } else {
                this.element.config.width.value = `100%`
            }
            this.element.config.imageDisplayFigure.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeTextLink(value) {
            this.element.config.textLink.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeFontSize(value) {
            this.element.config.fontSize.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeFontWeight(value) {
            this.element.config.fontWeight.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeLinkTextAlign(value) {
            helper.resetAllAlignValues(this.element);
            this.element.config.linkTextAlign.value = value;
            // text-bottom
            helper.setValuesForAlignBottom(value);
            //text-top
            helper.setValuesForAlignTop(value)
            //text-center
            helper.setValuesForAlignCenter(value)


            this.$emit('element-update', this.element);
        },
        onChangeTextMarginTop(value) {
            if( value !== null) {
                this.element.config.textMarginTop.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeTextMarginRight(value) {
            if (value !== null) {
                this.element.config.textMarginRight.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeTextMarginBottom(value) {
            if (value !== null) {
                this.element.config.textMarginBottom.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeTextMarginLeft(value) {
            if(value !== null) {
                this.element.config.textMarginLeft.value = value;

                this.$emit('element-update', this.element);
            }
        },
        onChangeImagePadding(value) {
          this.element.config.imagePadding.value = value;
          this.$emit('element-update', this.element);
        },
        onChangeMaxWidth(value) {
            this.element.config.maxWidth.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageCoverKeyOne(value) {
            if(!value) {
                this.element.config.imageCoverValueOne.value = null;
            }
            this.element.config.imageCoverKeyOne.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageCoverKeyTwo(value) {
            if(!value) {
                this.element.config.imageCoverValueTwo.value = null;
            }
            this.element.config.imageCoverKeyTwo.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageCoverValueOne(value) {
            // console.log('one: ', getCssUnitFromString(value));
            this.element.config.imageCoverValueOne.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageCoverValueOneCheck(event) {
            let value = event.target.value
            if(!isCssUnitAcceptable(getCssUnitFromString(value))) {
                value = getNumbersFromString(value);
                value += 'px';
            }
             if (!getNumbersFromString(value)) {
                 value = null;
             }
            this.element.config.imageCoverValueOne.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageCoverValueTwo(value) {
            // console.log('two: ', getCssUnitFromString(value));
            this.element.config.imageCoverValueTwo.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeImageCoverValueTwoCheck(event) {
            let value = event.target.value
            if(!isCssUnitAcceptable(getCssUnitFromString(value))) {
                value = getNumbersFromString(value);
                value += 'px';
            }
            if (!getNumbersFromString(value)) {
                value = null;
            }
            this.element.config.imageCoverValueTwo.value = value;
            this.$emit('element-update', this.element);
        }
    },
});
