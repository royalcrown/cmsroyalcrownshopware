import template from './sw-cms-el-preview-image-flex.html.twig';
import './sw-cms-el-preview-image-flex.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-image-flex', {template});