import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'custom-image',
    label: 'rc-cms.elements.custom-image',
    component: 'sw-cms-el-custom-image',
    configComponent: 'sw-cms-el-config-custom-image',
    previewComponent: 'sw-cms-el-preview-custom-image',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media',
            },
        },
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '250px',
        },
        maxWidth: {
            source: 'static',
            value: null,
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        imageBackgroundColor: {
            source: 'static',
            value: '#fffff00',
        },
        slideDirection: {
            source: 'static',
            value: 'bottom-up',
        },
        sliderHeight: {
          source: 'static',
          value: '45%'
        },
        amountOfLinesVisible: {
          source: 'static',
          value: '3'
        },
        linkColor: {
            source: 'static',
            value: '#000'
        },
        ellipsisColor: {
            source: 'static',
            value: '#000'
        },
        imageDisplayType: {
            source: 'static',
            value: 'cover'
        },
        imageDisplayFigure: {
            source: 'static',
            value: 'normal'
        },
        useTextOverlay: {
            source: 'static',
            value: false
        },
        useTextLink: {
            source: 'static',
            value: false
        },
        displayImageAsFigure: {
            source: 'static',
            value: false
        },
        width: {
            source: 'static',
            value: '100%'
        },
        linkTextAlign: {
            source: 'static',
            value: 'text-bottom left'
        },
        enableSlider: {
            source: 'static',
            value: true,
        },
        linkTextMarginTop: {
            source: 'static',
            value: null
        },
        linkTextMarginRight: {
            source: 'static',
            value: null
        },
        linkTextMarginLeft: {
            source: 'static',
            value: null
        },
        linkTextMarginBottom: {
            source: 'static',
            value: null
        },
        buttonText: {
            source: 'static',
            value: null,
        },
        text: {
            source: 'static',
            value: '<h3>Title</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>'
        },
        fontSize: {
            source: 'static',
            value: '0.78rem'
        },
        fontWeight: {
            source: 'static',
            value: '400',
        },
        textBackgroundColor: {
            source: 'static',
            value: '#fffacc95'
        },
        imageHorizontalPosition: {
            source: 'static',
            value: 'center',
        },
        imageVerticalPosition: {
            source: 'static',
            value: 'center',
        },
        textLink: {
            source: 'static',
            value: 'Enter text value'
        },
        textLinkUrl: {
            source: 'static',
            value: null
        },
        textLinkColor: {
            source: 'static',
            value: '#000'
        },
        textMarginTop: {
            source: 'static',
            value: null
        },
        textMarginRight: {
            source: 'static',
            value: null
        },
        textMarginLeft: {
            source: 'static',
            value: null
        },
        textMarginBottom: {
            source: 'static',
            value: null
        },
        textLinkNewTab: {
            source: 'static',
            value: false,
        },
        imagePadding: {
            source: 'static',
            value: '0'
        },
        imageCoverKeyOne: {
            source: 'static',
            value: null
        },
        imageCoverKeyTwo: {
            source: 'static',
            value: null
        },
        imageCoverValueOne: {
            source: 'static',
            value: '0px'
        },
        imageCoverValueTwo: {
            source: 'static',
            value: '0px'
        }
    },
});
