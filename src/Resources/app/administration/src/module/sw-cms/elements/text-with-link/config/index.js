import template from './sw-cms-el-config-text-with-link.html.twig';
import './sw-cms-el-config-text-with-link.scss';

const {Mixin, Component} = Shopware;

Component.register('sw-cms-el-config-text-with-link', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
        };
    },

    computed: {},

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('text-with-link');
        },

        onChangeMinHeight(value) {
            this.element.config.minHeight.value = value === null ? '' : value;

            this.$emit('element-update', this.element);
        },
        onBlur(content) {
            this.emitChanges(content);
        },
        onInput(content) {
            const el = document.createElement('div');
            el.innerHTML = content;
            const titleElements = el.querySelectorAll('h1, h2, h3, h4, h5, h6');
            titleElements.forEach(headerEl => el.removeChild(headerEl));
            const lastFontElement = Array.prototype.slice.call(el.querySelectorAll('FONT')).pop();

            if (lastFontElement) {
                if (this.element.config.ellipsisColor.value !== lastFontElement.getAttribute('color')) {
                    this.element.config.ellipsisColor.value = lastFontElement.getAttribute('color');
                }
            }
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
            }
            // console.log('on INPUT: ', content);
            this.$emit('element-update', this.element);
        },
        onChangeVisibleLines(value) {
            this.element.config.amountOfLinesVisible.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeButtonText(value) {
            this.element.config.buttonText.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeVerticalAlign(value) {
            this.element.config.verticalAlign.value = value;
            this.$emit('element-update', this.element);
        },
        emitChanges(content) {
            // console.log('on CHANGE EMITf: ', content);
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
                this.$emit('element-update', this.element);
            }
        },
    },
});
