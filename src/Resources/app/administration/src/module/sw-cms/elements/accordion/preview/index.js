import template from './sw-cms-el-preview-accordion.html.twig';
import './sw-cms-el-preview-accordion.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-accordion', {template});