import template from './sw-cms-el-config-image-text-sibling.html.twig';
import './sw-cms-el-config-image-text-sibling.scss';
const { Mixin, Component } = Shopware;

Component.register('sw-cms-el-config-image-text-sibling', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            imageFirst: true,
            initialFolderId: null,
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        previewSource() {
            if (this.element?.data?.media?.id) {
                return this.element.data.media;
            }

            return this.element.config.media.value;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('image-text-sibling');
        },

        async onImageUpload({ targetId }) {
            const mediaEntity = await this.mediaRepository.get(targetId);

            this.element.config.media.value = mediaEntity.id;
            this.element.config.media.source = 'static';

            this.updateElementData(mediaEntity);

            this.$emit('element-update', this.element);
        },

        onImageRemove() {
            this.element.config.media.value = null;

            this.updateElementData();

            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
        },
        onSwitchImageFirst(value) {
            this.imageFirst = value;
            this.onChangeFlexDirection(value ? 'row' : 'row-reverse');
        },
        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];
            this.element.config.media.value = media.id;
            this.element.config.media.source = 'static';

            this.updateElementData(media);

            this.$emit('element-update', this.element);
        },

        updateElementData(media = null) {
            const mediaId = media === null ? null : media.id;
            if (!this.element.data) {
                this.$set(this.element, 'data', { mediaId, media });
            } else {
                this.$set(this.element.data, 'mediaId', mediaId);
                this.$set(this.element.data, 'media', media);
            }
        },

        onOpenMediaModal() {
            this.mediaModalIsOpen = true;
        },

        onChangeMinHeight(value) {
            this.element.config.minHeight.value = value === null ? '' : value;

            this.$emit('element-update', this.element);
        },

        onChangeText(value) {
            if(value !== null){
                this.element.config.text.value = value;
            } else {
                this.element.config.text.value = '';
            }

            this.$emit('element-update', this.element);
        },
        onChangeRowHeight(value) {
            this.element.config.rowHeight.value = value === null ? '250px': value;
        },
        onChangeImageWidth(value) {
            if (value.includes('%')) {
                let width = value.split('%')[0];
                if(Number(width) > 80) {
                    width = '80%'
                }
                if(Number(width) < 20) {
                    width = '20%'
                }
                if (Number(width) >= 20 && Number(width) <= 80) {
                    width = value;
                }
                    this.element.config.imageWidth.value = width;
                this.$emit('element-update', this.element);
            }

        },
        onBlur(content) {
            this.emitChanges(content);
        },

        onInput(content) {
            this.emitChanges(content);
        },
        onChangeTextVerticalAlign(value) {
            this.element.config.textVerticalAlign.value = value;
            this.$emit('element-update', this.element);
        },

        onChangeFlexDirection(value) {
            this.element.config.flexDirection.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeButtonText(value) {
            this.element.config.buttonText.value = value;
            this.$emit('element-update', this.element);
        },
        emitChanges(content) {
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
                this.$emit('element-update', this.element);
            }
        },
    },
});
