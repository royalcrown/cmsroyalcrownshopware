import template from './sw-cms-el-custom-image.html.twig';
import './sw-cms-el-custom-image.scss';
import { mediaUrlHelper } from "../../../helperJs/imageUrlHelper";

const { Mixin, Component, Filter } = Shopware;

Component.register('sw-cms-el-custom-image', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        styles() {
            const backgroundColor = {'background-color': this.element.config.imageBackgroundColor.value};
            let height ={}
            if(this.element.config.minHeight.value && this.element.config.minHeight.value !== 0) {
                height = {'min-height': this.element.config.minHeight.value};
            }
            return {
                ...backgroundColor,
                ...height,
            }
        },
        displayModeClass() {
            let returnValue = ` is--${this.element.config.imageDisplayType.value}`;
            returnValue = returnValue +` is--${this.element.config.imageDisplayFigure.value}`;
            returnValue = returnValue + ` ${this.element.config.linkTextAlign.value}`;
            returnValue = returnValue + ` position-horizontal--${this.element.config.imageHorizontalPosition.value}`;
            returnValue = returnValue + ` position-vertical--${this.element.config.imageVerticalPosition.value}`;
            return returnValue
        },
        linkStyles() {
            if(this.element.config.useTextLink.value) {
                let color = {};
                let fontWeight = {};
                let fontSize = {};
                let marginRight = {};
                let marginLeft = {};
                let marginTop = {};
                let marginBottom = {};

                if (this.element.config.linkColor.value) {
                    color = {'color': this.element.config.textLinkColor.value}
                }

                if (this.element.config.fontWeight.value) {
                    fontWeight = {'font-weight': this.element.config.fontWeight.value}
                }

                if (this.element.config.fontSize.value) {
                    fontSize = {'font-size': this.element.config.fontSize.value,}
                }

                if (this.element.config.textMarginRight.value) {
                    marginRight = {'margin-right': this.element.config.textMarginRight.value,}
                }

                if (this.element.config.textMarginBottom.value) {
                    marginBottom = {'margin-bottom': this.element.config.textMarginBottom.value,}
                }

                if (this.element.config.textMarginLeft.value) {
                    marginLeft = {'margin-left': this.element.config.textMarginLeft.value,}
                }

                if (this.element.config.textMarginTop.value) {
                    marginTop = {'margin-top': this.element.config.textMarginTop.value,}
                }

                return {
                    ...color,
                    ...fontSize,
                    ...fontWeight,
                    ...marginTop,
                    ...marginBottom,
                    ...marginRight,
                    ...marginLeft
                };
            } else {
                return { 'display': 'none' }
            }
        },
        linkText() {
            return this.element.config.textLink.value || null
        },
        imgStyles() {
            let returnvalue = {
                'align-self': this.element.config.verticalAlign.value || null,
            };
            if(this.element.config.imageDisplayFigure.value !== 'normal') {
                const val = returnvalue
                returnvalue = {
                    ...val,
                    'width': this.element.config.width.value,
                    'height': this.element.config.minHeight.value,
                }
            } else {
                const val = returnvalue
                returnvalue = {
                    ...val,
                    'min-height': this.element.config.minHeight.value
                }
            }
            return returnvalue
        },

        imageMarkup() {
            return { 'padding': this.element.config.imagePadding.value,
                     'object-position': this.element.config.imageCoverKeyOne.value + ' ' + this.element.config.imageCoverValueOne.value + ' ' + this.element.config.imageCoverKeyTwo.value + ' ' + this.element.config.imageCoverValueTwo.value}
        },
        mediaUrl() {
            return mediaUrlHelper(this.element, Filter.getByName('asset'));
        },
        textContainerClasses(){
            let sliderEnabled = 'is--slider-enabled';
            if (!this.element.config.enableSlider.value) {
                sliderEnabled = 'is--slider-disabled';
            }

            let returnValue = `${this.element.config.slideDirection.value}`;
            returnValue = returnValue + ` ${sliderEnabled}`;

            if(`${this.element.config.useTextOverlay.value}` === 'true') {
                returnValue = returnValue + ` has-text`;
            }
            return returnValue;
        },
        textEditorStyles() {
          return {'--ellipsisColor': this.element.config.ellipsisColor.value}
        },
        textContainerMarkup() {
            const backgroundColor = {'background-color': this.element.config.textBackgroundColor.value};
            const sliderSize = { '--sliderSize': this.element.config.sliderHeight.value};
            const visibleLines = {'--visibleLineAmount': this.element.config.amountOfLinesVisible.value}
            return {
                ...backgroundColor,
                ...sliderSize,
                ...visibleLines,
            }
        },
        textStyles() {
            return {'': ''}
        },
        getButtonClasses() {
            let buttonClass = 'is--overlay-slider-button'
            if (!this.element.config.enableSlider.value) {
                buttonClass = 'is--overlay-visible-button';
            }
            return buttonClass;
        },
        linkButtonStyles() {
          let returnValue;
          returnValue = { 'color': this.element.config.linkColor.value}
            return returnValue;
        },
        mediaConfigValue() {
            return this.element?.config?.sliderItems?.value;
        },
    },
    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },

        mediaConfigValue(value) {
            const mediaId = this.element?.data?.media?.id;
            const isSourceStatic = this.element?.config?.media?.source === 'static';

            if (isSourceStatic && mediaId && value !== mediaId) {
                this.element.config.media.value = mediaId;
            }
        },
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.initElementConfig('custom-image');
            this.initElementData('custom-image');
        },

        onBlur(content) {
            this.emitChanges(content);
        },
        onInput(content) {
            this.emitChanges(content);
        },
        emitChanges(content) {
            if (content !== this.element.config.text.value) {
                this.element.config.content.text.value = content;
                this.$emit('element-update', this.element);
            }
        },
    },
});