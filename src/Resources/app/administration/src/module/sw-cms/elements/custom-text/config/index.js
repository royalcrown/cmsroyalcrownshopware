import template from './sw-cms-el-config-custom-text.html.twig';
import './sw-cms-el-config-custom-text.scss';
import {getNumbersFromString} from "../../../helperJs/numbersHelper";

const {Mixin, Component} = Shopware;

Component.register('sw-cms-el-config-custom-text', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            useButton: false,
            useTextLink: false,
        };
    },
    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('custom-text');
            this.useButton = this.element.config.useButton.value;
            this.useTextLink = this.element.config.useTextLink.value;
        },
        onChangeMinHeight(value) {
            if (getNumbersFromString(value) > 49) {
                this.element.config.minHeight.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeLinkOrButtonText(value) {
            this.element.config.linkOrButtonText.value = value;
            this.$emit('element-update', this.element);
        },
        onContentChange(content) {
            if (content !== this.element.config.text.value) {
                this.element.config.text.value = content;
                this.$emit('element-update', this.element);
            }
        },
        onChangeUseTextLink(value) {
            this.useTextLink = value
            this.element.config.useTextLink.value = value;
            if (this.useButton) {
                this.element.config.useButton.value = !value;
                this.useButton = !value;
            }
            this.$emit('element-update', this.element);
        },
        onChangeUseButton(value) {
            this.useButton = value
            this.element.config.useButton.value = value;
            if (this.useTextLink) {
                this.element.config.useTextLink.value = !value;
                this.useTextLink = !value;
            }
            this.$emit('element-update', this.element);
        },
        onChangeFontSize(value) {
            this.element.config.fontSize.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeFontWeight(value) {
            this.element.config.fontWeight.value = value;
            this.$emit('element-update', this.element);
        },
    },
});
