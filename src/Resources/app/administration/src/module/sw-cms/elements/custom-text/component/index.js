import template from './sw-cms-el-custom-text.html.twig';
import './sw-cms-el-custom-text.scss';

const { Mixin, Component, Filter } = Shopware;

Component.register('sw-cms-el-custom-text', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        styles() {
            let optional= {};
            const backgroundColor = {'background-color': this.element.config.backgroundColor.value};
            let height ={}
            if(this.element.config.minHeight.value && this.element.config.minHeight.value !== 0) {
                height = {'min-height': this.element.config.minHeight.value};
            }

            if(this.element.config.useButton.value || this.element.config.useTextLink.value) {
                optional = {'flex-direction': 'column', 'padding-top': '25px'}
            }
            return {
                ...backgroundColor,
                ...height,
                ...optional,
                'display': 'flex',
                'padding-left': '25px',
                'padding-right': '25px'
            }
        },
        linkStyles() {
            return {'color': this.element.config.linkColor.value,
            'margin-top': '20xp',
                'margin-bottom': '20px',
            'font-size': this.element.config.fontSize.value,
            'font-weight': this.element.config.fontWeight.value};
        },
        buttonStyles() {
            return {
                'margin-top': '20px',
                'margin-bottom': '20px',
                // 'height': '30px',
                'width': 'fit-content',
                'padding': '3px 10px',
                'display': 'flex',
                'justify-content': 'center',
                'align-items': 'center',
                'background-color': this.element.config.buttonColor.value,
                'color': this.element.config.linkColor.value,
                'font-size': this.element.config.fontSize.value,
                'font-weight': this.element.config.fontWeight.value
            }
        },
        linkOrButtonText() {
            return this.element.config.linkOrButtonText.value;
        },
        textStyles() {

            return {
                'align-self': this.element.config.verticalAlign.value || null,
            }
        },
        linkClasses() {
            return `is--${this.element.config.linkPosition.value}`
        },
        getButtonClasses() {
            return 'is--custom-text-button ' + `is--${this.element.config.linkPosition.value}`;
        },
    },
    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.initElementConfig('custom-text');
        },
    },
});