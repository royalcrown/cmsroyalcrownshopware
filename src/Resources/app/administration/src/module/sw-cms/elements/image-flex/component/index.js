import CMS from '../../../constant/sw-cms.constant';
import template from './sw-cms-el-image-flex.html.twig';
import './sw-cms-el-image-flex.scss';
import {getNumbersFromString} from "../../../helperJs/numbersHelper";
import {getCssUnitFromString} from "../../../helperJs/cssUnitHelper";

const {Mixin, Component, Filter} = Shopware;

Component.register('sw-cms-el-image-flex', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        displayModeClass() {
            if (this.element.config.displayMode.value === 'standard') {
                return null;
            }
            let returnValue = `is--${this.element.config.displayMode.value}`;
            returnValue = returnValue + ` ${this.element.config.textAlign.value}`;
            // return `is--${this.element.config.displayMode.value}`;
            if (this.element.config.imageDisplayType.value === 'standard') {
                return returnValue
            }
            return returnValue + ` is--display-type-${this.element.config.imageDisplayType.value}`;
        },
        styles() {
            const parentElement = document.getElementById(this.element.id)?.parentElement;
            if ([...parentElement?.children].length === 2) {
                [...parentElement.children].forEach(el => {
                    if (el.id === this.element.id) {
                        el.style.width = this.element.config.imageWidth.value;
                    } else {
                        const width = getNumbersFromString(this.element.config.imageWidth.value)
                        el.style.width = `${100 - width}${getCssUnitFromString(this.element.config.imageWidth.value)}`;
                    }
                });
                return {
                    'min-height': this.element.config.displayMode.value === 'cover' &&
                    this.element.config.minHeight.value &&
                    this.element.config.minHeight.value !== 0 ? this.element.config.minHeight.value : '250px;',
                };

            }
        },

        imgStyles() {
            return {
                'align-self': this.element.config.verticalAlign.value || null,
                'object-fit': this.element.config.imageDisplayType.value,
            };
        },
        linkStyles() {
            let color = {};
            let fontWeight = {};
            let fontSize = {};
            let marginRight = {};
            let marginLeft = {};
            let marginTop = {};
            let marginBottom = {};

            if (this.element.config.linkColor.value) {
                color = {'color': this.element.config.linkColor.value}
            }

            if (this.element.config.fontWeight.value) {
                fontWeight = {'font-weight': this.element.config.fontWeight.value}
            }

            if (this.element.config.fontSize.value) {
                fontSize = {'font-size': this.element.config.fontSize.value,}
            }

            if (this.element.config.textMarginRight.value) {
                marginRight = {'margin-right': this.element.config.textMarginRight.value,}
            }

            if (this.element.config.textMarginBottom.value) {
                marginBottom = {'margin-bottom': this.element.config.textMarginBottom.value,}
            }

            if (this.element.config.textMarginLeft.value) {
                marginLeft = {'margin-left': this.element.config.textMarginLeft.value,}
            }

            if (this.element.config.textMarginTop.value) {
                marginTop = {'margin-top': this.element.config.textMarginTop.value,}
            }

            return {
                ...color,
                ...fontSize,
                ...fontWeight,
                ...marginTop,
                ...marginBottom,
                ...marginRight,
                ...marginLeft
            };
        },
        text() {
            return this.element.config.text.value || null
        },

        mediaUrl() {
            const fallBackImageFileName = CMS.MEDIA.previewMountain.slice(CMS.MEDIA.previewMountain.lastIndexOf('/') + 1);
            const staticFallBackImage = this.assetFilter(`administration/static/img/cms/${fallBackImageFileName}`);
            const elemData = this.element.data.media;
            const elemConfig = this.element.config.media;

            if (elemConfig.source === 'mapped') {
                const demoMedia = this.getDemoValue(elemConfig.value);

                if (demoMedia?.url) {
                    return demoMedia.url;
                }

                return staticFallBackImage;
            }

            if (elemConfig.source === 'default') {
                // use only the filename
                const fileName = elemConfig.value.slice(elemConfig.value.lastIndexOf('/') + 1);
                return this.assetFilter(`/administration/static/img/cms/${fileName}`);
            }

            if (elemData?.id) {
                return this.element.data.media.url;
            }

            if (elemData?.url) {
                return this.assetFilter(elemConfig.url);
            }

            return staticFallBackImage;
        },

        assetFilter() {
            return Filter.getByName('asset');
        },

        mediaConfigValue() {
            return this.element?.config?.sliderItems?.value;
        },
    },

    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },

        mediaConfigValue(value) {
            const mediaId = this.element?.data?.media?.id;
            const isSourceStatic = this.element?.config?.media?.source === 'static';

            if (isSourceStatic && mediaId && value !== mediaId) {
                this.element.config.media.value = mediaId;
            }
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('image-flex');
            this.initElementData('image-flex');
        },
    },
});
