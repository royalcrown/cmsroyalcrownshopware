import template from './sw-cms-el-config-accordion.html.twig';
import './sw-cms-el-config-accordion.scss';
import {isCssUnitPx} from "../../../helperJs/cssUnitHelper";

const {Mixin, Component} = Shopware;

Component.register('sw-cms-el-config-accordion', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            useButton: false,
            useTextLink: false,
            useImage: false,
        };
    },
    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('accordion');
            this.useButton = this.element.config.useButton.value;
            this.useTextLink = this.element.config.useTextLink.value;
            this.useImage = this.element.config.useImage.value;
        },
        onContentChange(value, index) {
            if (value !== this.element.config.entries.value[index].content) {
                this.element.config.entries.value[index].content = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeItemTitle(value, index) {
            this.element.config.entries.value[index].title = value;
            this.$emit('element-update', this.element);
        },
        addItem() {
            this.element.config.entries.value.push({title: null,
                content: null,
                btnBackground: 'transparent',
                btnColor: '#000',
                panelBackground: '#fff',
                btnRadius: '0 0 0 0',
                verticalAlign: null,
                fontSize: '0.87rem',
                fontWeight: '400'
            });
        },
        removeItem(index) {
            const foundIndex = this.element.config.entries.value.findIndex((el, i) => i === index);
            if (foundIndex > -1) {
                this.element.config.entries.value.splice(index, 1);
                this.$emit('element-update', this.element);
            }
        },
        onChangeFontSize(value, index) {
            this.element.config.entries.value[index].fontSize = value;
            this.$emit('element-update', this.element);
        },
        onChangeFontWeight(value, index) {
            this.element.config.entries.value[index].fontWeight = value;
            this.$emit('element-update', this.element);
        },
        onChangeButtonRadius(value, index) {
            this.element.config.entries.value[index].btnRadius = value;
            this.$emit('element-update', this.element);
        }
    },
});
