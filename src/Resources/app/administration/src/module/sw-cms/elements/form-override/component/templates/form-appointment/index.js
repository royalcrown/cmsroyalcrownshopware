import template from './sw-cms-el-form-appointment.html.twig';

export default {
    template,
    props: ['formSettings'],
};
