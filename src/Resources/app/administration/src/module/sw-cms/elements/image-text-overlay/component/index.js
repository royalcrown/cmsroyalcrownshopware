import template from './sw-cms-el-image-text-overlay.html.twig';
import './sw-cms-el-image-text-overlay.scss';
import { mediaUrlHelper } from "../../../helperJs/imageUrlHelper";

const { Mixin, Component, Filter } = Shopware;

Component.register('sw-cms-el-image-text-overlay', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        styles() {
            return {
                'min-height': this.element.config.displayMode.value === 'cover' &&
                this.element.config.minHeight.value &&
                this.element.config.minHeight.value !== 0 ? this.element.config.minHeight.value : '340px;',
            };
        },
        displayModeClass() {
            if (this.element.config.displayMode.value === 'standard') {
                return null;
            }
            let returnValue = `is--${this.element.config.displayMode.value}`;
            returnValue = returnValue + ` ${this.element.config.linkTextAlign.value}`;

            // return `is--${this.element.config.displayMode.value}`;
            if (this.element.config.imageDisplayType.value === 'standard') {
                return returnValue
            }
            return returnValue + ` is--display-type-${this.element.config.imageDisplayType.value}`;
        },
        imgStyles() {
            return {
                'align-self': this.element.config.verticalAlign.value || null,
            };
        },
        mediaUrl() {
            return mediaUrlHelper(this.element, Filter.getByName('asset'));
        },
        textContainerClasses(){
            let sliderEnabled = 'is--slider-enabled';
            if (!this.element.config.enableSlider.value) {
                sliderEnabled = 'is--slider-disabled';
            }

            let returnValue = `${this.element.config.slideDirection.value}`;
            returnValue = returnValue + ` ${sliderEnabled}`;
            return returnValue;
        },
        textEditorStyles() {
          return {'--ellipsisColor': this.element.config.ellipsisColor.value}
        },
        textContainerMarkup() {
            const backgroundColor = {'background-color': this.element.config.textBackgroundColor.value};
            const sliderSize = { '--sliderSize': this.element.config.sliderHeight.value};
            const visibleLines = {'--visibleLineAmount': this.element.config.amountOfLinesVisible.value}
            return {
                ...backgroundColor,
                ...sliderSize,
                ...visibleLines,
            }
        },
        textStyles() {
            return {'': ''}
        },
        getButtonClasses() {
            let buttonClass = 'is--overlay-slider-button'
            if (!this.element.config.enableSlider.value) {
                buttonClass = 'is--overlay-visible-button';
            }
            return buttonClass;
        },
        linkButtonStyles() {
          let returnValue;
          returnValue = { 'color': this.element.config.linkColor.value}
            return returnValue;
        },
        mediaConfigValue() {
            return this.element?.config?.sliderItems?.value;
        },
    },
    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },

        mediaConfigValue(value) {
            const mediaId = this.element?.data?.media?.id;
            const isSourceStatic = this.element?.config?.media?.source === 'static';

            if (isSourceStatic && mediaId && value !== mediaId) {
                this.element.config.media.value = mediaId;
            }
        },
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.initElementConfig('image-text-overlay');
            this.initElementData('image-text-overlay');
        },

        onBlur(content) {
            this.emitChanges(content);
        },
        onInput(content) {
            this.emitChanges(content);
        },
        emitChanges(content) {
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
                this.$emit('element-update', this.element);
            }
        },
    },
});