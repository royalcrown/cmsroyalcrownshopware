import template from './sw-cms-el-preview-double-image.html.twig';
import './sw-cms-el-preview-double-image.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-double-image', {template});