import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'image-link',
    label: 'rc-cms.elements.image-text-link',
    component: 'sw-cms-el-image-link',
    configComponent: 'sw-cms-el-config-image-link',
    previewComponent: 'sw-cms-el-preview-image-link',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media',
            },
        },
        displayMode: {
            source: 'static',
            value: 'standard',
        },
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '340px',
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        linkColor: {
            source: 'static',
            value: '#000'
        },
        text: {
            source: 'static',
            value: 'Enter text'
        },
        imageDisplayType: {
            source: 'static',
            value: 'standard'
        },
        fontSize: {
            source: 'static',
            value: '1rem'
        },
        fontWeight: {
            source: 'static',
            value: '400',
        },

        textAlign: {
            source: 'static',
            value: 'text-bottom left'
        },

        textMarginTop: {
            source: 'static',
            value: null
        },
        textMarginRight: {
            source: 'static',
            value: null
        },
        textMarginLeft: {
            source: 'static',
            value: null
        },
        textMarginBottom: {
            source: 'static',
            value: null
        },
    },
});
