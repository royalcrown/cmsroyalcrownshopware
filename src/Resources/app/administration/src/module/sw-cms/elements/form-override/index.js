import './component';
import './config';

const { Service } = Shopware;
const cmsElements = Service('cmsService').getCmsElementRegistry();

const cmsElementForm = cmsElements['form'];


if(!cmsElementForm.defaultConfig.backgroundColor) {
    cmsElementForm.defaultConfig =  {
        ...cmsElementForm.defaultConfig,
        backgroundColor: {
            source: 'static',
            value: 'transparent'
        }
    }
}