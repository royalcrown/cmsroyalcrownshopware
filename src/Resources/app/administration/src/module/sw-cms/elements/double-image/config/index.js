import template from './sw-cms-el-config-double-image.html.twig';
import './sw-cms-el-config-double-image.scss';
import {getNumbersFromString} from "../../../helperJs/numbersHelper";
import {getCssUnitFromString} from "../../../helperJs/cssUnitHelper";

const {Mixin, Component} = Shopware;

Component.register('sw-cms-el-config-double-image', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
            activeProperty: null,
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        previewSourceLeft() {
            if (this.element?.data?.mediaLeft?.id) {
                return this.element.data.mediaLeft;
            }

            return this.element.config.mediaLeft.value;
        },
        previewSourceRight() {
            if (this.element?.data?.mediaRight?.id) {
                return this.element.data.mediaRight;
            }

            return this.element.config.mediaRight.value;
        },

    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('double-image');
        },

        async onImageUploadLeft({targetId}) {
            const mediaEntity = await this.mediaRepository.get(targetId);

            this.element.config.mediaLeft.value = mediaEntity.id;
            this.element.config.mediaLeft.source = 'static';
            this.updateElementData(mediaEntity);

            this.updateElement()
        },
        async onImageUploadRight({targetId}) {
            const mediaEntity = await this.mediaRepository.get(targetId);

            this.element.config.mediaRight.value = mediaEntity.id;
            this.element.config.mediaRight.source = 'static';

            this.updateElementData(mediaEntity);

            this.updateElement()
        },

        onImageRemove(property) {
            this.element.config[property].value = null;

            this.updateElementData();

            this.updateElement()
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
            this.activeProperty = null;
        },

        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];
            this.element.config[this.activeProperty].value = media.id;
            this.element.config[this.activeProperty].source = 'static';

            this.updateElementData(media, this.activeProperty);

            this.updateElement()
        },

        updateElementData(media = null, property = 'mediaLeft') {
            const mediaId = media === null ? null : media.id;
            if (!this.element.data) {
                this.$set(this.element, 'data', {});
            }
                this.$set(this.element.data, `${property}Id`, mediaId);
                this.$set(this.element.data, property, media);
        },

        onOpenMediaModal(value) {
            this.activeProperty = value;
            this.mediaModalIsOpen = true;
        },
        onChangeImageWidthLeft(value) {
            if(value.includes('%')) {
                const restWidth = `${100 - getNumbersFromString(value)}${getCssUnitFromString(value)}`;
                this.element.config.leftImageWidth.value = value;
                this.element.config.rightImageWidth.value = restWidth
                this.updateElement()
            }
        },
        onChangeImageWidthRight(value) {
            if(value.includes('%')) {
                const restWidth = `${100 - getNumbersFromString(value)}${getCssUnitFromString(value)}`;
                this.element.config.leftImageWidth.value = restWidth;
                this.element.config.rightImageWidth.value = value
                this.updateElement()
            }
        },
        onChangeDisplayModeLeft(value) {
            this.element.config.displayModeLeft.value = value;
            this.updateElement()
        },
        onChangeDisplayModeRight(value) {
            this.element.config.displayModeRight.value = value;
            this.updateElement()
        },
        onChangeMinHeightRightImage(value) {
            if(getCssUnitFromString(value) !== '') {
                this.element.config.minHeightLeftImage.value = value;
                this.element.config.minHeightRightImage.value = value;
                this.updateElement()
            }
        },
        onChangeMinHeightLeftImage(value) {
            if(getCssUnitFromString(value) !== '') {
                this.element.config.minHeightRightImage.value = value;
                this.element.config.minHeightLeftImage.value = value;
                this.updateElement()
            }
        },
        updateElement() {
            this.$emit('element-update', this.element);
        }
    },
});
