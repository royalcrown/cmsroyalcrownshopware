import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'text-with-link',
    label: 'rc-cms.elements.text-with-link',
    component: 'sw-cms-el-text-with-link',
    configComponent: 'sw-cms-el-config-text-with-link',
    previewComponent: 'sw-cms-el-preview-text-with-link',
    defaultConfig: {
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '200px',
        },
        verticalAlign: {
            source: 'static',
            value: 'start',
        },
        amountOfLinesVisible: {
          source: 'static',
          value: '3'
        },
        linkColor: {
            source: 'static',
            value: '#000'
        },
        ellipsisColor: {
            source: 'static',
            value: '#000'
        },
        buttonText: {
            source: 'static',
            value: null,
        },
        content: {
            source: 'static',
            value: '<h3>Title</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>'
        },
        textBackgroundColor: {
            source: 'static',
            value: '#fffacc95'
        },
    },
});
