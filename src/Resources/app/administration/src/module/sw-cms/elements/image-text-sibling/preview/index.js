import template from './sw-cms-el-preview-image-text-sibling.html.twig';
import './sw-cms-el-preview-image-text-sibling.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-image-text-sibling', {template});