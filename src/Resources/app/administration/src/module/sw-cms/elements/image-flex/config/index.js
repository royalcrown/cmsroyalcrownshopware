import template from './sw-cms-el-config-image-flex.html.twig';
import './sw-cms-el-config-image-flex.scss';
import * as helper from './helper';

const {Mixin, Component} = Shopware;

Component.register('sw-cms-el-config-image-flex', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        previewSource() {
            if (this.element?.data?.media?.id) {
                return this.element.data.media;
            }

            return this.element.config.media.value;
        },

    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('image-flex');
        },
        setWidthThroughListener(width) {
            this.element.config.imageWidth.value = width;
        },

        async onImageUpload({targetId}) {
            const mediaEntity = await this.mediaRepository.get(targetId);

            this.element.config.media.value = mediaEntity.id;
            this.element.config.media.source = 'static';

            this.updateElementData(mediaEntity);

            this.$emit('element-update', this.element);
        },

        onImageRemove() {
            this.element.config.media.value = null;

            this.updateElementData();

            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
        },

        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];
            this.element.config.media.value = media.id;
            this.element.config.media.source = 'static';

            this.updateElementData(media);

            this.$emit('element-update', this.element);
        },

        updateElementData(media = null) {
            const mediaId = media === null ? null : media.id;
            if (!this.element.data) {
                this.$set(this.element, 'data', {mediaId, media});
            } else {
                this.$set(this.element.data, 'mediaId', mediaId);
                this.$set(this.element.data, 'media', media);
            }
        },

        onOpenMediaModal() {
            this.mediaModalIsOpen = true;
        },

        onChangeMinHeight(value) {
            this.element.config.minHeight.value = value === null ? '' : value;

            this.$emit('element-update', this.element);
        },

        onChangeDisplayMode(value) {
            // if (value === 'cover') {
                this.element.config.verticalAlign.value = null;
            // }

            this.$emit('element-update', this.element);
        },

        onChangeText(value) {
            if (value !== null) {
                this.element.config.text.value = value;
            } else {
                this.element.config.text.value = '';
            }

            this.$emit('element-update', this.element);
        },

        onChangeImageDisplayType(value) {

            if (value !== 'standard') {
                this.element.config.minHeight.value = `${document.getElementById(this.element.id).getBoundingClientRect().width}px`;
            } else {
                this.element.config.minHeight.value = '250px';
            }

            this.$emit('element-update', this.element);
        },

        onChangeTextWeight(value) {
            this.element.style.borderColor = 'green';
            this.element.style.borderLeft = '3px';
            this.element.config.fontWeight.value = value;

            this.$emit('element-update', this.element);
        },
        onChangeFontSize(value) {
            this.element.config.fontSize.value = value;

            this.$emit('element-update', this.element);
        },
        onChangeImageWidth(value) {
            this.element.config.imageWidth.value = value;
            this.$emit('element-update', this.element);

        },
        submitEventListenerTest(value) {
            const customEvent = new CustomEvent('test', {value: value});
            this.dispatchEvent(customEvent);
        },
        onChangeTextAlign(value) {
            helper.resetAllAlignValues(this.element);
            this.element.config.textAlign.value = value;
            // text-bottom
            helper.setValuesForAlignBottom(value);
            //text-top
            helper.setValuesForAlignTop(value)
            //text-center
            helper.setValuesForAlignCenter(value)


            this.$emit('element-update', this.element)
        },
        onChangeTextMarginTop(value) {
            if (value !== null) {
                this.element.config.textMarginTop.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeTextMarginRight(value) {
            if (value !== null) {
                this.element.config.textMarginRight.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeTextMarginBottom(value) {
            if (value !== null) {
                this.element.config.textMarginBottom.value = value;
                this.$emit('element-update', this.element);
            }
        },
        onChangeTextMarginLeft(value) {
            if (value !== null) {
                this.element.config.textMarginLeft.value = value;
                this.$emit('element-update', this.element);
            }
        }
    },
});
