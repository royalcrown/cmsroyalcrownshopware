import template from './sw-cms-el-preview-text-with-link.html.twig';
import './sw-cms-el-preview-text-with-link.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-text-with-link', {template});