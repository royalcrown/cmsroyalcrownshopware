const setValuesForAlignBottom = (value) => {
    if (value === 'text-bottom left') {
        document.getElementById('sw-field--element-config-textMarginTop-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginRight-value').disabled = true;
    }

    if (value === 'text-bottom right') {

        document.getElementById('sw-field--element-config-textMarginTop-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginLeft-value').disabled = true;
    }

    if (value === 'text-bottom center') {
        document.getElementById('sw-field--element-config-textMarginTop-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginLeft-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginRight-value').disabled = true;
    }
};

const  setValuesForAlignCenter = (value) => {
    if (value === 'text-center left') {
        document.getElementById('sw-field--element-config-textMarginBottom-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginTop-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginRight-value').disabled = true;
    }

    if (value === 'text-center right') {
        document.getElementById('sw-field--element-config-textMarginBottom-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginTop-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginLeft-value').disabled = true;
    }

    if (value === 'text-center center') {
        document.getElementById('sw-field--element-config-textMarginTop-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginBottom-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginLeft-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginRight-value').disabled = true;
    }
}

const setValuesForAlignTop = (value) => {
    if (value === 'text-top left') {
        document.getElementById('sw-field--element-config-textMarginBottom-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginRight-value').disabled = true;
    }

    if (value === 'text-top right') {
        document.getElementById('sw-field--element-config-textMarginBottom-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginLeft-value').disabled = true;
    }

    if (value === 'text-top center') {
        document.getElementById('sw-field--element-config-textMarginBottom-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginLeft-value').disabled = true;
        document.getElementById('sw-field--element-config-textMarginRight-value').disabled = true;
    }
};

const resetAllAlignValues = (el) => {
    el.config.textMarginTop.value = '';
    el.config.textMarginBottom.value = '';
    el.config.textMarginRight.value = '';
    el.config.textMarginLeft.value = '';
    document.getElementById('sw-field--element-config-textMarginTop-value').disabled = false;
    document.getElementById('sw-field--element-config-textMarginLeft-value').disabled = false;
    document.getElementById('sw-field--element-config-textMarginRight-value').disabled = false;
    document.getElementById('sw-field--element-config-textMarginBottom-value').disabled = false;

}

export { setValuesForAlignBottom, setValuesForAlignCenter, setValuesForAlignTop, resetAllAlignValues }