import CMS from '../../../constant/sw-cms.constant';
import template from './sw-cms-el-double-image.html.twig';
import './sw-cms-el-double-image.scss';
import {getNumbersFromString} from "../../../helperJs/numbersHelper";
import {getCssUnitFromString} from "../../../helperJs/cssUnitHelper";

const {Mixin, Component, Filter} = Shopware;

Component.register('sw-cms-el-double-image', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        mediaUrlLeft() {
            const fallBackImageFileName = CMS.MEDIA.previewMountain.slice(CMS.MEDIA.previewMountain.lastIndexOf('/') + 1);
            const staticFallBackImage = this.assetFilter(`administration/static/img/cms/${fallBackImageFileName}`);
            const elemData = this.element.data.mediaLeft;
            const elemConfig = this.element.config.mediaLeft;

            if (elemConfig.source === 'mapped') {
                const demoMedia = this.getDemoValue(elemConfig.value);

                if (demoMedia?.url) {
                    return demoMedia.url;
                }

                return staticFallBackImage;
            }

            if (elemConfig.source === 'default') {
                // use only the filename
                const fileName = elemConfig.value.slice(elemConfig.value.lastIndexOf('/') + 1);
                return this.assetFilter(`/administration/static/img/cms/${fileName}`);
            }

            if (elemData?.id) {
                return this.element.data.mediaLeft.url;
            }

            if (elemData?.url) {
                return this.assetFilter(elemConfig.url);
            }

            return staticFallBackImage;
        },
        mediaUrlRight() {
            const fallBackImageFileName = CMS.MEDIA.previewCamera.slice(CMS.MEDIA.previewCamera.lastIndexOf('/') + 1);
            const staticFallBackImage = this.assetFilter(`administration/static/img/cms/${fallBackImageFileName}`);
            const elemData = this.element.data.mediaRight;
            const elemConfig = this.element.config.mediaRight;

            if (elemConfig.source === 'mapped') {
                const demoMedia = this.getDemoValue(elemConfig.value);

                if (demoMedia?.url) {
                    return demoMedia.url;
                }

                return staticFallBackImage;
            }

            if (elemConfig.source === 'default') {
                // use only the filename
                const fileName = elemConfig.value.slice(elemConfig.value.lastIndexOf('/') + 1);
                return this.assetFilter(`/administration/static/img/cms/${fileName}`);
            }

            if (elemData?.id) {
                return this.element.data.mediaRight.url;
            }

            if (elemData?.url) {
                return this.assetFilter(elemConfig.url);
            }

            return staticFallBackImage;
        },

        assetFilter() {
            return Filter.getByName('asset');
        },

        mediaConfigValue() {
            return this.element?.config?.sliderItems?.value;
        },

        styles() {
            const unit = getCssUnitFromString(this.element.config.minHeightLeftImage.value);
            const leftHeight = getNumbersFromString(this.element.config.minHeightLeftImage.value);
            const rightHeight = getNumbersFromString(this.element.config.minHeightRightImage.value);
            const height = leftHeight > rightHeight ? leftHeight : rightHeight;
            return {
                'display': 'flex',
                'height': `${height}${unit}`,
            }
        },
        imageContainerLeftStyles() {
            return {
                'width': this.element.config.leftImageWidth.value,
                'background-color': this.element.config.backgroundColorLeft.value
            }
        },
        imageContainerRightClasses() {
            return `is-display-${this.element.config.displayModeRight.value}`
        },
        imageContainerLeftClasses() {
            return `is-display-${this.element.config.displayModeLeft.value}`
        },
        imageLeftStyles() {
        },
        imageContainerRightStyles() {
            return {
                'width': this.element.config.rightImageWidth.value,
                'background-color': this.element.config.backgroundColorRight.value}
        },
        imageRightStyles() {
        }
    },

    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },

        mediaConfigValue(value) {
            const mediaId = this.element?.data?.media?.id;
            const isSourceStatic = this.element?.config?.media?.source === 'static';

            if (isSourceStatic && mediaId && value !== mediaId) {
                this.element.config.media.value = mediaId;
            }
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('double-image');
            this.initElementData('double-image');
        },
    },
});
