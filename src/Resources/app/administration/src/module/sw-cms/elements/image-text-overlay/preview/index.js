import template from './sw-cms-el-preview-image-text-overlay.html.twig';
import './sw-cms-el-preview-image-text-overlay.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-image-text-overlay', {template});