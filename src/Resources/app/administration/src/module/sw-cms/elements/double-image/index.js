import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'double-image',
    label: 'rc-cms.elements.double-image',
    component: 'sw-cms-el-double-image',
    configComponent: 'sw-cms-el-config-double-image',
    previewComponent: 'sw-cms-el-preview-double-image',
    defaultConfig: {
        mediaLeft: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media',
            },
        },
        mediaRight: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media',
            },
        },
        displayModeLeft: {
            source: 'static',
            value: 'standard',
        },
        displayModeRight: {
            source: 'static',
            value: 'standard',
        },
        leftUrl: {
            source: 'static',
            value: null,
        },
        rightUrl: {
            source: 'static',
            value: null,
        },
        newTabLeft: {
            source: 'static',
            value: false,
        },
        newTabRight: {
            source: 'static',
            value: false,
        },
        minHeightRightImage: {
            source: 'static',
            value: '250px',
        },
        minHeightLeftImage: {
            source: 'static',
            value: '250px',
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        leftImageWidth: {
            source: 'static',
            value: '50%'
        },
        rightImageWidth: {
            source: 'static',
            value: '50%'
        },
        backgroundColorLeft: {
            source: 'static',
            value: null,
        },
        backgroundColorRight: {
            source: 'static',
            value: null,
        }

    },
});
