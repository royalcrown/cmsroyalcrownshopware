import template from './sw-cms-el-preview-image-link.html.twig';
import './sw-cms-el-preview-image-link.scss';


const {Component} = Shopware;
Component.register('sw-cms-el-preview-image-link', {template});