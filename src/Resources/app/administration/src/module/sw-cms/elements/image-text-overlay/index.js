import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'image-text-overlay',
    label: 'rc-cms.elements.image-text-overlay',
    component: 'sw-cms-el-image-text-overlay',
    configComponent: 'sw-cms-el-config-image-text-overlay',
    previewComponent: 'sw-cms-el-preview-image-text-overlay',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media',
            },
        },
        displayMode: {
            source: 'static',
            value: 'standard',
        },
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '340px',
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        slideDirection: {
            source: 'static',
            value: 'bottom-up',
        },
        sliderHeight: {
          source: 'static',
          value: '45%'
        },
        amountOfLinesVisible: {
          source: 'static',
          value: '3'
        },
        linkColor: {
            source: 'static',
            value: '#000'
        },
        ellipsisColor: {
            source: 'static',
            value: '#000'
        },
        imageDisplayType: {
            source: 'static',
            value: 'standard'
        },

        linkTextAlign: {
            source: 'static',
            value: 'text-bottom left'
        },
        enableSlider: {
            source: 'static',
            value: true,
        },
        linkTextMarginTop: {
            source: 'static',
            value: null
        },
        linkTextMarginRight: {
            source: 'static',
            value: null
        },
        linkTextMarginLeft: {
            source: 'static',
            value: null
        },
        linkTextMarginBottom: {
            source: 'static',
            value: null
        },
        buttonText: {
            source: 'static',
            value: null,
        },
        content: {
            source: 'static',
            value: '<h3>Title</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>'
        },
        textBackgroundColor: {
            source: 'static',
            value: '#fffacc95'
        },
    },
});
