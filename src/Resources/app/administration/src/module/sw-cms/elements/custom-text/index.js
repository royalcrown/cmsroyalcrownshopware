import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'custom-text',
    label: 'rc-cms.elements.custom-text',
    component: 'sw-cms-el-custom-text',
    configComponent: 'sw-cms-el-config-custom-text',
    previewComponent: 'sw-cms-el-preview-custom-text',
    defaultConfig: {
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '250px',
        },
        verticalAlign: {
            source: 'static',
            value: null,
        },
        backgroundColor: {
            source: 'static',
            value: '#fffff00',
        },
        amountOfLinesVisible: {
            source: 'static',
            value: '3'
        },
        buttonColor: {
            source: 'static',
            value: '#ccc'
        },
        buttonTextColor: {
            source: 'static',
            value: '#000'
        },
        ellipsisColor: {
            source: 'static',
            value: '#000'
        },
        useTextLink: {
            source: 'static',
            value: false
        },
        useButton: {
            source: 'static',
            value: false
        },
        width: {
            source: 'static',
            value: '100%'
        },
        linkTextAlign: {
            source: 'static',
            value: 'text-bottom left'
        },
        linkTextMarginTop: {
            source: 'static',
            value: null
        },
        linkOrButtonText: {
            source: 'static',
            value: null,
        },
        text: {
            source: 'static',
            value: '<h3>Title</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>'
        },
        fontSize: {
            source: 'static',
            value: '0.78rem'
        },
        fontWeight: {
            source: 'static',
            value: '400',
        },
        linkColor: {
            source: 'static',
            value: '#000'
        },
        linkPosition: {
            source: 'static',
            value: 'center'
        }
    },
});
