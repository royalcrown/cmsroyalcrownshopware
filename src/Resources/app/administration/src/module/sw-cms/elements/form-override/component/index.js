import template from './sw-cms-el-custom-form.html.twig';
import appointment from './templates/form-appointment/index'

const {Component} = Shopware;

Component.override('sw-cms-el-form', {
        template,

        components: {
            appointment,
        },
        computed: {
            styles() {
                return {'background-color': this.element.config.backgroundColor.value}
            },
        },
    }
);
