import  './component';
import  './config'
import  './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'image-text-sibling',
    label: 'rc-cms.elements.image-text',
    component: 'sw-cms-el-image-text-sibling',
    configComponent: 'sw-cms-el-config-image-text-sibling',
    previewComponent: 'sw-cms-el-preview-image-text-sibling',
    defaultConfig: {
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media',
            },
        },
        url: {
            source: 'static',
            value: null,
        },
        newTab: {
            source: 'static',
            value: false,
        },
        minHeight: {
            source: 'static',
            value: '250px',
        },
        rowHeight: {
            source: 'static',
            value: '250px'
        },
        flexDirection: {
            source: 'static',
            value: 'row'
        },
        imageWidth: {
            source: 'static',
            value: '33%'
        },
        content: {
            source: 'static',
            value: 'sibling text'
        },
        verticalAlign: {
            source: 'static',
            value: 'center',
        },
        textBackgroundColor: {
            source: 'static',
            value: 'transparent'
        },
        textVerticalAlign: {
            source: 'static',
            value: 'flex-start'
        },
        buttonText: {
            source: 'static',
            value: null,
        }
    },
});
