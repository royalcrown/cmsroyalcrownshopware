import CMS from '../../../constant/sw-cms.constant';
import template from './sw-cms-el-image-text-sibling.html.twig';
import './sw-cms-el-image-text-sibling.scss';

const {Mixin, Component, Filter} = Shopware;

Component.register('sw-cms-el-image-text-sibling', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        styles() {
            let returnValue = {};
            const flexDirection = {
                'flex-direction': this.element.config.flexDirection.value
            }

            const minHeight = {
                'min-height': this.element.config.minHeight.value
            }
            return {
                ...flexDirection,
                ...minHeight
            };
        },
        getButtonClasses() {
            return `is-${this.element.config.verticalAlign.value}`;
        },
        rowStyles() {
            const rowHeight = {
                'height': this.element.config.rowHeight.value
            }
            const imageWidth = {
                'width': this.element.config.imageWidth.value,
            }
            return {
                ...rowHeight,
                ...imageWidth
            }
        },
        textContainerMarkup() {
            const backgroundColor = {'background-color': this.element.config.textBackgroundColor.value};
            const value = 100 - Number(this.element.config.imageWidth.value.split('%')[0]);
            const maxWidth = { 'width': `${value}%`}
            const displayFlex = {'display': 'flex'}
            const flexDirection = {'flex-direction': 'column'}
            const verticalAlign = { 'justify-content': this.element.config.textVerticalAlign.value }
            return {
                ...displayFlex,
                ...flexDirection,
                ...verticalAlign,
                ...backgroundColor,
                ...maxWidth
            }
        },
        textStyles() {
            const width = {'width': '100%'}

            return {
                ...width,
            }
        },
        mediaUrl() {
            const fallBackImageFileName = CMS.MEDIA.previewMountain.slice(CMS.MEDIA.previewMountain.lastIndexOf('/') + 1);
            const staticFallBackImage = this.assetFilter(`administration/static/img/cms/${fallBackImageFileName}`);
            const elemData = this.element.data.media;
            const elemConfig = this.element.config.media;

            if (elemConfig.source === 'mapped') {
                const demoMedia = this.getDemoValue(elemConfig.value);

                if (demoMedia?.url) {
                    return demoMedia.url;
                }

                return staticFallBackImage;
            }

            if (elemConfig.source === 'default') {
                // use only the filename
                const fileName = elemConfig.value.slice(elemConfig.value.lastIndexOf('/') + 1);
                return this.assetFilter(`/administration/static/img/cms/${fileName}`);
            }

            if (elemData?.id) {
                return this.element.data.media.url;
            }

            if (elemData?.url) {
                return this.assetFilter(elemConfig.url);
            }

            return staticFallBackImage;
        },
        assetFilter() {
            return Filter.getByName('asset');
        },

        mediaConfigValue() {
            return this.element?.config?.sliderItems?.value;
        },
        siblingText() {
            return this.element.config.content.value;
        }
    },

    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },

        mediaConfigValue(value) {
            const mediaId = this.element?.data?.media?.id;
            const isSourceStatic = this.element?.config?.media?.source === 'static';

            if (isSourceStatic && mediaId && value !== mediaId) {
                this.element.config.media.value = mediaId;
            }
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('image-text-sibling');
            this.initElementData('image-text-sibling');
        },

        onBlur(content) {
            this.emitChanges(content);
        },

        onInput(content) {
            this.emitChanges(content);
        },
        emitChanges(content) {
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
                this.$emit('element-update', this.element);
            }
        },
    },
});
