import template from './sw-cms-el-text-with-link.html.twig';
import './sw-cms-el-text-with-link.scss';

const { Mixin, Component } = Shopware;

Component.register('sw-cms-el-text-with-link', {
    template,

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    computed: {
        styles() {
            const minHeight =  {
                'min-height': this.element.config.minHeight.value
            };
            return {
                ...minHeight
            }
        },
        textEditorStyles() {
            const ellipsisColor = {'--ellipsisColor': this.element.config.ellipsisColor.value};

          return {
                ...ellipsisColor
          }
        },
        textContainerMarkup() {
            const backgroundColor = {'background-color': this.element.config.textBackgroundColor.value};
            const visibleLines = {'--visibleLineAmount': this.element.config.amountOfLinesVisible.value};
            const textAlign = {'align-content': this.element.config.verticalAlign.value};
            return {
                ...backgroundColor,
                ...visibleLines,
                ...textAlign
            }
        },
        linkButtonStyles() {
          let returnValue;
          returnValue = { 'color': this.element.config.linkColor.value}
            return returnValue;
        },
    },
    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            },
        },
    },
    created() {
        this.createdComponent();
    },
    methods: {
        createdComponent() {
            this.initElementConfig('text-with-link');
            this.initElementData('text-with-link');
        },

        onBlur(content) {
            this.emitChanges(content);
        },
        onInput(content) {
            this.emitChanges(content);
        },
        emitChanges(content) {
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
                this.$emit('element-update', this.element);
            }
        },
    },
});