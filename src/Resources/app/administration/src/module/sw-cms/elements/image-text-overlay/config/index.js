import template from './sw-cms-el-config-image-text-overlay.html.twig';
import './sw-cms-el-config-image-text-overlay.scss';
const { Mixin, Component } = Shopware;

Component.register('sw-cms-el-config-image-text-overlay', {
    template,

    inject: ['repositoryFactory'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            mediaModalIsOpen: false,
            initialFolderId: null,
        };
    },

    computed: {
        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        uploadTag() {
            return `cms-element-media-config-${this.element.id}`;
        },

        previewSource() {
            if (this.element?.data?.media?.id) {
                return this.element.data.media;
            }

            return this.element.config.media.value;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('image-text-overlay');
        },

        async onImageUpload({ targetId }) {
            const mediaEntity = await this.mediaRepository.get(targetId);

            this.element.config.media.value = mediaEntity.id;
            this.element.config.media.source = 'static';

            this.updateElementData(mediaEntity);

            this.$emit('element-update', this.element);
        },

        onImageRemove() {
            this.element.config.media.value = null;

            this.updateElementData();

            this.$emit('element-update', this.element);
        },

        onCloseModal() {
            this.mediaModalIsOpen = false;
        },

        onSelectionChanges(mediaEntity) {
            const media = mediaEntity[0];
            this.element.config.media.value = media.id;
            this.element.config.media.source = 'static';

            this.updateElementData(media);

            this.$emit('element-update', this.element);
        },

        updateElementData(media = null) {
            const mediaId = media === null ? null : media.id;
            if (!this.element.data) {
                this.$set(this.element, 'data', { mediaId, media });
            } else {
                this.$set(this.element.data, 'mediaId', mediaId);
                this.$set(this.element.data, 'media', media);
            }
        },

        onOpenMediaModal() {
            this.mediaModalIsOpen = true;
        },

        onChangeMinHeight(value) {
            this.element.config.minHeight.value = value === null ? '' : value;

            this.$emit('element-update', this.element);
        },

        onChangeDisplayMode(value) {
            if (value === 'cover') {
                this.element.config.verticalAlign.value = null;
            }

            this.$emit('element-update', this.element);
        },

        onChangeSlideDirection(value) {
            this.element.config.slideDirection.value = value;
            this.$emit('element-update', this.element);
        },

        onChangeImageDisplayType(value) {

            if (value !== 'standard') {
                this.element.config.minHeight.value =  `${document.getElementById(this.element.id).getBoundingClientRect().width}px`;
            } else {
                this.element.config.minHeight.value = '340px';
            }

            this.$emit('element-update', this.element);
        },

        onBlur(content) {
            this.emitChanges(content);
        },
        onInput(content) {
            const el = document.createElement('div');
            el.innerHTML = content;
            const titleElements = el.querySelectorAll('h1, h2, h3, h4, h5, h6');
            titleElements.forEach(headerEl => el.removeChild(headerEl));
            const lastFontElement = Array.prototype.slice.call(el.querySelectorAll('FONT')).pop();

            if(lastFontElement) {
                if (this.element.config.ellipsisColor.value !== lastFontElement.getAttribute('color')) {
                    this.element.config.ellipsisColor.value = lastFontElement.getAttribute('color');
                }
            }
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
            }
            // console.log('on INPUT: ', content);
            this.$emit('element-update', this.element);
        },
        onChangeSliderHeightText(value) {
            this.element.config.sliderHeight.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeVisibleLines(value) {
            this.element.config.amountOfLinesVisible.value = value;
            this.$emit('element-update', this.element);
        },
        onChangeButtonText(value) {
            this.element.config.buttonText.value = value;
            this.$emit('element-update', this.element);
        },
        emitChanges(content) {
            // console.log('on CHANGE EMITf: ', content);
            if (content !== this.element.config.content.value) {
                this.element.config.content.value = content;
                this.$emit('element-update', this.element);
            }
        },
    },
});
