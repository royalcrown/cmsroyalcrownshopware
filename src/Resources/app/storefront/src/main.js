// Import all necessary Storefront plugins
import ImageLinkPlugin from "./imagelink-plugin/imagelink.plugin";
import ImageFlexPlugin from "./imageFlex-plugin/imageFlex.plugin";

// Register your plugin via the existing PluginManager
const PluginManager = window.PluginManager;

PluginManager.register('ImageLinkPlugin', ImageLinkPlugin, '[data-imagelink-plugin]');
PluginManager.register('ImageFlexPlugin', ImageFlexPlugin, '[data-imageflex-plugin]');