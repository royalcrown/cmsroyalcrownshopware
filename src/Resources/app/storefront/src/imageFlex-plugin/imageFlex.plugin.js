import Plugin from 'src/plugin-system/plugin.class';
export default class ImageFlexPlugin extends Plugin {
    init() {
        window.addEventListener('load', this.setWidthForImageSiblingElements.bind(this));
    }

    setWidthForImageSiblingElements() {
        const cmsImageContainer = this.el.closest('.cms-element-image-text-sibling');
        const width = getComputedStyle(cmsImageContainer)
            .getPropertyValue('--width');
        if(cmsImageContainer && width) {
            cmsImageContainer.parentElement.style.width= width;
        }
    }
}