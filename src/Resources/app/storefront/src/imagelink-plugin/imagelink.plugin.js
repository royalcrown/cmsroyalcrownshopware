import Plugin from 'src/plugin-system/plugin.class';

export default class ImageLinkPlugin extends Plugin {
    init() {
        window.addEventListener('load', this.calculateHeightAndWidth.bind(this));
        window.addEventListener('resize', this.calculateHeightAndWidth.bind(this));
    }

    calculateHeightAndWidth() {

        // if inside gridblock, padding is determined
        if (this.el.parentElement.classList.contains('grid-block')) {
            const maxWidth = Number([...this.el.classList].find((x) => x.includes('maxWidth-')).replace(/[^0-9]/g, ""));
            const cmsImageContainer = this.el.querySelector('.cms-image-container');
            const container = this.el.parentElement;
            const smallerThan900 = window.screen.width < 900
            if(smallerThan900) {
                // container.style.minHeight = cmsImageContainer.style.minHeight;
                container.style.maxHeight = cmsImageContainer.style.minHeight
            }

            if (container && this.el.classList.contains('is--square') || this.el.classList.contains('is--circle')) {
                if(this.el.classList.contains('position-horizontal--center')) {
                    container.classList.add('d-flex');
                    container.classList.add('justify-content-center');
                }
                if(this.el.classList.contains('position-horizontal--right')) {
                    container.classList.add('d-flex');
                    container.classList.add('justify-content-end');
                }
                container.classList.remove('no-custom-figure');
                container.classList.add('custom-figure');
                const paddingX = Number(window.getComputedStyle(container, null).getPropertyValue('padding-left').replace(/[^0-9]/g, ""));
                const paddingY = Number(window.getComputedStyle(container, null).getPropertyValue('padding-bottom').replace(/[^0-9]/g, ""));
                const bounds = container.getBoundingClientRect();

                let width = bounds.width - (paddingX * 2);
                const height = bounds.height - (paddingY * 2);
                if(maxWidth && width > maxWidth) {
                    width = maxWidth;
                }
                if (height > width) {
                    this.el.style.width = `${height}px`;
                    this.el.style.height = `${height}px`;
                }
                if (width > height) {
                    if (smallerThan900) {
                        this.el.style.width = `${cmsImageContainer.style.minHeight}`;
                        this.el.style.height = `${cmsImageContainer.style.minHeight}`;
                        this.el.style.margin = 'auto';
                    } else {

                        this.el.style.width = `${width}px`;
                        this.el.style.height = `${width}px`;
                    }
                }
                if (width === height) {
                    this.el.style.width = `${width}px`;
                    this.el.style.height = `${width}px`;
                }
            }
        } else {
            const cmsImageContainer = this.el.querySelector('.cms-image-container');
            if (cmsImageContainer) {
                if (cmsImageContainer && cmsImageContainer.classList.contains('is-square') || cmsImageContainer.classList.contains('is-circle')) {
                    const imgContainer = this.el.querySelector('.cms-image-container');

                    const bounds = imgContainer.getBoundingClientRect();
                    if (bounds.height && bounds.height < bounds.width) {
                        imgContainer.style.width = `${bounds.height}px`;
                    }
                    if (bounds.width < bounds.height) {
                        imgContainer.style.height = `${bounds.width}px`;
                    }
                }
            }
        }
    }
}