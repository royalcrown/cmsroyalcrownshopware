<?php

namespace CmsRoyalCrownShopware\Storefront\Controller;

use CmsRoyalCrownShopware\Core\Content\AppointmentForm\SalesChannel\AbstractAppointmentFormRoute;
use Shopware\Core\Framework\RateLimiter\Exception\RateLimitExceededException;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\Framework\Validation\Exception\ConstraintViolationException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(defaults={"_routeScope"={"storefront"}})
 */
class CustomFormController extends StorefrontController
{
    public function __construct(
        private readonly AbstractAppointmentFormRoute $appointmentFormRoute
    )
    {
    }

    /**
     * @Route("/form/appointment", name="frontend.form.appointment.send", methods={"POST"}, defaults={"XmlHttpRequest"= true, "_captcha"= true})
     */
    public function sendAppointmentForm(RequestDataBag $data, SalesChannelContext $context): JsonResponse
    {

        $response = [];
        try {
            $message = $this->appointmentFormRoute
                ->load($data->toRequestDataBag(), $context)
                ->getResult()
                ->getIndividualSuccessMessage();
            if (!$message) {
                $message = $this->trans('appointment.success');
            }
            $response[] = [
                'type' => 'success',
                'alert' => $message,
            ];
        } catch (ConstraintViolationException $formViolations) {
            $violations = [];
            foreach ($formViolations->getViolations() as $violation) {
                $violations[] = $violation->getMessage();
            }
            $response[] = [
                'type' => 'danger',
                'alert' => $this->renderView('@Storefront/storefront/utilities/alert.html.twig', [
                    'type' => 'danger',
                    'list' => $violations,
                ]),
            ];
        } catch (RateLimitExceededException $exception) {
            $response[] = [
                'type' => 'info',
                'alert' => $this->renderView('@Storefront/storefront/utilities/alert.html.twig', [
                    'type' => 'info',
                    'content' => $this->trans('error.rateLimitExceeded', ['%seconds%' => $exception->getWaitTime()]),
                ]),
            ];
        }
//        dd(new JsonResponse($response));
        return new JsonResponse($response);
    }
}