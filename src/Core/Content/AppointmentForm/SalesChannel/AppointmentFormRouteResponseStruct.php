<?php declare(strict_types=1);

namespace CmsRoyalCrownShopware\Core\Content\AppointmentForm\SalesChannel;

use Shopware\Core\Framework\Struct\Struct;

class AppointmentFormRouteResponseStruct extends Struct
{
    /**
     * @var string
     */
    protected $individualSuccessMessage;

    public function getApiAlias(): string
    {
        return 'appointment_form_result';
    }

    public function getIndividualSuccessMessage(): string
    {
        return $this->individualSuccessMessage;
    }
}
