<?php declare(strict_types=1);

namespace CmsRoyalCrownShopware\Core\Content\AppointmentForm\SalesChannel;

use Shopware\Core\System\SalesChannel\StoreApiResponse;

class AppointmentFormRouteResponse extends StoreApiResponse
{
    /**
     * @var AppointmentFormRouteResponseStruct
     */
    protected $object;

    public function __construct(AppointmentFormRouteResponseStruct $object)
    {
        parent::__construct($object);
    }

    public function getResult(): AppointmentFormRouteResponseStruct
    {
        return $this->object;
    }
}
