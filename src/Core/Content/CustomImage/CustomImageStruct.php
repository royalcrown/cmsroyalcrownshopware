<?php

namespace CmsRoyalCrownShopware\Core\Content\CustomImage;

use Shopware\Core\Content\Cms\SalesChannel\Struct\ImageStruct;
use Shopware\Core\Framework\Struct\Struct;

class CustomImageStruct extends Struct
{
    /**
     * @var ImageStruct
     */
    protected $imageStruct;

    /**
     * @return ImageStruct
     */
    public function getImageStruct(): ImageStruct
    {
        return $this->imageStruct;
    }

    /**
     * @param ImageStruct $imageStruct
     */
    public function setImageStruct(ImageStruct $imageStruct): void
    {
        $this->imageStruct = $imageStruct;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition(string $position): void
    {
        $this->position = $position;
    }
    /**
     * @var string
     */
    protected $position;


}