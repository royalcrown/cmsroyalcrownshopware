<?php

namespace CmsRoyalCrownShopware\Core\Content\Flow\Subscriber;

use CmsRoyalCrownShopware\Core\Content\Flow\Events\AppointmentFormEvent;
use CmsRoyalCrownShopware\Core\useShopware\Core\Framework\Event\Event\EmailAware;
use Shopware\Core\Framework\Event\BusinessEventCollector;
use Shopware\Core\Framework\Event\BusinessEventCollectorEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BusinessEventCollectorSubscriber implements EventSubscriberInterface
{
    private BusinessEventCollector $businessEventCollector;

    public function __construct(BusinessEventCollector $businessEventCollector) {
        $this->businessEventCollector = $businessEventCollector;
    }

    public static function getSubscribedEvents()
    {
        return [
            BusinessEventCollectorEvent::NAME => ['onAddAppointmentFormEvent', 1000],
        ];
    }

    public function onAddAppointmentFormEvent(BusinessEventCollectorEvent $event): void
    {
        $collection = $event->getCollection();

        $definition = $this->businessEventCollector->define(AppointmentFormEvent::class);

        if (!$definition) {
            return;
        }

        $collection->set($definition->getName(), $definition);
    }
}