<?php declare(strict_types=1);

namespace CmsRoyalCrownShopware\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Content\Flow\Aggregate\FlowTemplate\FlowTemplateDefinition;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\Doctrine\MultiInsertQueryQueue;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;

class Migration1695975250 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1695975250;
    }

    public function update(Connection $connection): void
    {
        $existingFlowTemplates = $this->getExistingFlowTemplates($connection);

        $mailTemplates = $this->getDefaultMailTemplates($connection);
        $eventActions = $this->getDefaultEventActions();

        $flowTemplates = [];
        $createdAt = (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT);

        foreach ($eventActions as $eventAction) {
            $templateName = $this->getEventFullNameByEventName($eventAction['event_name']);

            if (\in_array($templateName, $existingFlowTemplates, true)) {
                continue;
            }

            $flowTemplate = [
                'id' => Uuid::randomBytes(),
                'name' => $templateName,
                'created_at' => $createdAt,
            ];

            $config = !\array_key_exists($eventAction['mail_template_type'], $mailTemplates) ? null
                : $this->getConfigData($mailTemplates[$eventAction['mail_template_type']]);

            $flowTemplate['config'] = json_encode([
                'eventName' => $eventAction['event_name'],
                'description' => null,
                'customFields' => null,
                'sequences' => [
                    [
                        'id' => Uuid::randomHex(),
                        'actionName' => 'action.mail.send',
                        'config' => $config,
                        'parentId' => null,
                        'ruleId' => null,
                        'position' => 1,
                        'trueCase' => 0,
                        'displayGroup' => 1,
                    ],
                ],
            ], \JSON_THROW_ON_ERROR);

            $flowTemplates[] = $flowTemplate;
        }

        $queue = new MultiInsertQueryQueue($connection);

        foreach ($flowTemplates as $flowTemplate) {
            $queue->addInsert(FlowTemplateDefinition::ENTITY_NAME, $flowTemplate);
        }

        $queue->execute();
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    /**
     * @return array<int|string, array<string, mixed>>
     */
    private function getDefaultMailTemplates(Connection $connection): array
    {
        $mailTemplates = $connection->fetchAllAssociative('
                SELECT LOWER(HEX(mail_template.id)) as mail_template_id,
                       LOWER(HEX(mail_template.mail_template_type_id)) as mail_template_type_id,
                       mail_template_type.technical_name
                FROM mail_template
                INNER JOIN mail_template_type ON mail_template_type.id = mail_template.mail_template_type_id
                WHERE mail_template.system_default = 1
        ');

        $result = [];
        foreach ($mailTemplates as $mailTemplate) {
            $result[$mailTemplate['technical_name']] = [
                'mail_template_id' => $mailTemplate['mail_template_id'],
                'mail_template_type_id' => $mailTemplate['mail_template_type_id'],
            ];
        }

        return $result;
    }

    /**
     * @param array<string, string> $mailTemplateData
     *
     * @return array<string, mixed>
     */
    private function getConfigData(array $mailTemplateData): array
    {
        $config = [];
        foreach ($mailTemplateData as $key => $value) {
            $key = lcfirst(implode('', array_map('ucfirst', explode('_', $key))));
            $config[$key] = $value;
        }

        $config['recipient'] = ['data' => [], 'type' => 'default'];

        return $config;
    }

    /**
     * @return array<array<string, string>>
     */
    private function getDefaultEventActions(): array
    {
        return [
            [
                'event_name' => 'appointment_form.send',
                'mail_template_type' => 'appointment_form',
            ],
        ];
    }

    private function getEventFullNameByEventName(string $eventName): string
    {
        $listEventName = [
            'appointment_form.send' => 'Appointment form sent',
        ];

        if (\array_key_exists($eventName, $listEventName)) {
            return $listEventName[$eventName];
        }

        return $eventName;
    }

    /**
     * @return string[]
     */
    private function getExistingFlowTemplates(Connection $connection): array
    {
        /** @var string[] $flowTemplates */
        $flowTemplates = $connection->fetchFirstColumn('SELECT DISTINCT name FROM flow_template');

        return array_unique(array_filter($flowTemplates));
    }
}
