<?php declare(strict_types=1);

use Shopware\Core\TestBootstrapper;

$loader = (new TestBootstrapper())
    ->addCallingPlugin()
    ->addActivePlugins('CmsRoyalCrownShopware')
    ->bootstrap()
    ->getClassLoader();

$loader->addPsr4('CmsRoyalCrownShopware\\Tests\\', __DIR__);